package br.ufrn.imd.forum.Forum.rest;

import br.ufrn.imd.forum.Forum.core.auth.users.UsuarioForum;
import br.ufrn.imd.forum.Forum.core.utils.PageResponse;
import br.ufrn.imd.forum.Forum.core.utils.QueryFilter;
import br.ufrn.imd.forum.Forum.domain.Duvida;
import br.ufrn.imd.forum.Forum.dto.DuvidaDTO;
import br.ufrn.imd.forum.Forum.exception.BusinessRuleException;
import br.ufrn.imd.forum.Forum.mapper.DuvidaMapper;
import br.ufrn.imd.forum.Forum.service.DuvidaService;
import br.ufrn.imd.forum.Forum.service.NotificacaoService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/duvida")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", maxAge = 3600)
public class DuvidaRestController {
    private final DuvidaService duvidaService;
    private final NotificacaoService notificacaoService;

    @GetMapping
    public PageResponse<DuvidaDTO> buscarTodos(
            @RequestParam(value = "search", required = false) String search,
            @RequestParam(value = "id_related", required = false) Long idRelacionado,
            @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @RequestParam(value = "pageSize", required = false, defaultValue = "20") int pageSize
    ) {
        QueryFilter filtro = new QueryFilter(search, idRelacionado, page, pageSize);
        PageResponse<Duvida> duvidas = this.duvidaService.buscarTodos(filtro);
        PageResponse<DuvidaDTO> response = new PageResponse<>(duvidas);

        response.setConteudo(
                duvidas.getConteudo().stream().map(DuvidaMapper::converter).collect(Collectors.toList())
        );

        return response;
    }

    @GetMapping(path = "/{id}")
    public DuvidaDTO buscarUm(@PathVariable("id") Long idTopico) {
        return DuvidaMapper.converter(this.duvidaService.buscar(idTopico));
    }

    @PostMapping(path = "/tag")
    public List<DuvidaDTO> buscarPorTag(@RequestBody List<Long> idsTags) {
        List<Duvida> duvidas = this.duvidaService.buscarPorTags(idsTags);
        return duvidas.stream().map(DuvidaMapper::converter).collect(Collectors.toList());
    }

    @PostMapping
    public Long cadastrar(UsuarioForum user, @RequestBody DuvidaDTO duvidaDTO) throws BusinessRuleException {
        Duvida duvida = DuvidaMapper.converter(duvidaDTO);
        Duvida duvidaBd = this.duvidaService.cadastrar(user.getId(), duvida);
        try {
            this.notificacaoService.notificar(duvidaBd);
        } catch (MessagingException e) {
            // Faça nada, email não enviado não deve ser notificado para usuário que ta tentando cadastrar duvida
        }
        return duvidaBd.getId();
    }

    @PutMapping
    public Long editar(UsuarioForum user, @RequestBody DuvidaDTO duvidaDTO) throws BusinessRuleException {
        Duvida duvida = DuvidaMapper.converter(duvidaDTO);
        return this.duvidaService.editar(user.getId(), duvida).getId();
    }

    @DeleteMapping(path = "/{id}")
    public void deletar(UsuarioForum user, @PathVariable("id") Long idDuvida) throws BusinessRuleException {
        this.duvidaService.deletar(user.getId(), idDuvida);
    }

    @PostMapping(path = "/{id}/marcar-como-respondida")
    public void marcarComoRespondida(UsuarioForum user, @PathVariable("id") Long idDuvida, @RequestParam("id_resposta") Long idResposta) throws BusinessRuleException {
        this.duvidaService.marcarComoRespondida(user.getId(), idDuvida, idResposta);
    }
}
