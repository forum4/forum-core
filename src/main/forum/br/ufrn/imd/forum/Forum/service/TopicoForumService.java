package br.ufrn.imd.forum.Forum.service;

import br.ufrn.imd.forum.Forum.core.utils.PageResponse;
import br.ufrn.imd.forum.Forum.core.utils.PermissaoValidatorUtil;
import br.ufrn.imd.forum.Forum.core.utils.QueryFilter;
import br.ufrn.imd.forum.Forum.core.utils.ValidatorUtil;
import br.ufrn.imd.forum.Forum.domain.Pessoa;
import br.ufrn.imd.forum.Forum.domain.Tag;
import br.ufrn.imd.forum.Forum.domain.TopicoForum;
import br.ufrn.imd.forum.Forum.exception.BusinessRuleException;
import br.ufrn.imd.forum.Forum.repository.TopicoForumRepository;
import br.ufrn.imd.forum.Forum.service.interfaces.ServiceInterfaceProtected;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TopicoForumService implements ServiceInterfaceProtected<TopicoForum, Long> {
    private final TopicoForumRepository topicoForumRepository;
    private final PessoaService pessoaService;
    private final TagService tagService;

    @Override
    public TopicoForum salvar(Long idUsuario, TopicoForum entidade) throws BusinessRuleException {
        this.validarTopicoForum(entidade);

        TopicoForum topicoBd = new TopicoForum();
        Pessoa pessoa = this.pessoaService.buscar(idUsuario);

        if (ValidatorUtil.isNotEmpty(entidade.getId())) {
            topicoBd = this.buscar(entidade.getId());
            topicoBd.setDataUltimaEdicao(new Date());
        } else {
            topicoBd.setPessoaCadastro(pessoa);
        }

        BeanUtils.copyProperties(entidade, topicoBd, TopicoForum.ignoreProperties);
        topicoBd.setPessoaUltimaEdicao(pessoa);
        this.validarTopicoForum(topicoBd);
        return this.topicoForumRepository.save(topicoBd);
    }

    /**
     * Valida um tópico de fórum
     *
     * @param topico - Tópico a ser validado
     * @throws BusinessRuleException - Se tópico não for válido
     */
    private void validarTopicoForum(TopicoForum topico) throws BusinessRuleException {
        if (ValidatorUtil.isEmpty(topico.getTitulo())) {
            throw new BusinessRuleException("Titulo do tópico não pode ser vazio");
        }

        if (ValidatorUtil.isEmpty(topico.getDescricao())) {
            throw new BusinessRuleException("Descrição do tópico não pode ser vazia");
        }

        if (ValidatorUtil.isNotEmpty(topico.getId()) && ValidatorUtil.isEmpty(topico.getPessoaCadastro())) {
            throw new BusinessRuleException("Tópico não associada a um usuário");
        }
    }

    @Override
    public PageResponse<TopicoForum> buscarTodos(QueryFilter filtro) {
        PageResponse<TopicoForum> response = new PageResponse<>();
        int skipRows = (filtro.getPage() - 1) * filtro.getPageSize();

        if (ValidatorUtil.isEmpty(filtro.getSearch())) {
            filtro.setSearch("%");
        } else {
            filtro.setSearch("%" + filtro.getSearch().toLowerCase() + "%");
        }

        List<TopicoForum> topicos = this.topicoForumRepository.findAllByTituloOrDescricaoPageable(filtro.getSearch(), filtro.getPageSize(), skipRows);
        Integer total = this.topicoForumRepository.countDistinctByTituloOrDescricaoPageable(filtro.getSearch());

        if (ValidatorUtil.isEmpty(total)) {
            total = 0;
        }

        response.setConteudo(topicos);
        response.setTamanhoPagina(filtro.getPageSize());
        response.setPaginaAtual(filtro.getPage());
        response.setTotalElementos(total.longValue());
        response.setTotalPaginas((long) (total / filtro.getPageSize()));

        return response;
    }

    @Override
    public TopicoForum buscar(Long id) {
        return this.topicoForumRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Tópico de Fórum não encontrado"));
    }

    public List<TopicoForum> buscarPorTags(List<Long> idsTags) {
        List<Tag> tags = idsTags.stream().map(tagService::buscar).collect(Collectors.toList());
        return this.topicoForumRepository.findDistinctByTagsInAndAtivoTrueOrderByDataCriado(tags);
    }

    @Override
    public void deletar(Long idUsuario, Long id) throws BusinessRuleException {
        Pessoa pessoa = this.pessoaService.buscar(idUsuario);
        TopicoForum topicoForum = this.buscar(id);
        PermissaoValidatorUtil.permiteExcluir(pessoa, topicoForum.getPessoaCadastro().getId());
        topicoForum.setPessoaUltimaEdicao(pessoa);
        this.desativarTopico(topicoForum);

    }

    @Transactional
    public void desativarTopico(TopicoForum topicoForum) {
        topicoForum.setAtivo(false);
        this.topicoForumRepository.save(topicoForum);
    }
}
