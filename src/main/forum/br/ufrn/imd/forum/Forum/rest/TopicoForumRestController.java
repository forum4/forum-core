package br.ufrn.imd.forum.Forum.rest;

import br.ufrn.imd.forum.Forum.core.auth.users.UsuarioForum;
import br.ufrn.imd.forum.Forum.core.utils.PageResponse;
import br.ufrn.imd.forum.Forum.core.utils.QueryFilter;
import br.ufrn.imd.forum.Forum.core.utils.ValidatorUtil;
import br.ufrn.imd.forum.Forum.domain.Pessoa;
import br.ufrn.imd.forum.Forum.domain.TopicoForum;
import br.ufrn.imd.forum.Forum.dto.TopicoForumDTO;
import br.ufrn.imd.forum.Forum.exception.BusinessRuleException;
import br.ufrn.imd.forum.Forum.mapper.TopicoForumMapper;
import br.ufrn.imd.forum.Forum.service.TopicoForumService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/topico-forum")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", maxAge = 3600)
public class TopicoForumRestController {
    private final TopicoForumService topicoForumService;

    @GetMapping
    public PageResponse<TopicoForumDTO> buscarTodos(
            @RequestParam(value = "search", required = false) String search,
            @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @RequestParam(value = "pageSize", required = false, defaultValue = "20") int pageSize
    ) {
        QueryFilter filtro = new QueryFilter(search, null, page, pageSize);

        PageResponse<TopicoForum> topicos = this.topicoForumService.buscarTodos(filtro);
        PageResponse<TopicoForumDTO> resposta = new PageResponse<>(topicos);

        resposta.setConteudo(
                topicos.getConteudo().stream().map(TopicoForumMapper::converter).collect(Collectors.toList())
        );
        return resposta;
    }

    @GetMapping(path = "/{id}")
    public TopicoForumDTO buscarUm(@PathVariable("id") Long idTopico) {
        return TopicoForumMapper.converter(this.topicoForumService.buscar(idTopico));
    }

    @PostMapping(path = "/tag")
    public List<TopicoForumDTO> buscarPorTag(@RequestBody List<Long> idsTags) {
        List<TopicoForum> topicos = this.topicoForumService.buscarPorTags(idsTags);
        return topicos.stream().map(TopicoForumMapper::converter).collect(Collectors.toList());
    }

    @PostMapping
    public Long salvar(UsuarioForum user, @RequestBody TopicoForumDTO topico) throws BusinessRuleException {
        TopicoForum topicoForum = TopicoForumMapper.converter(topico);

        if (ValidatorUtil.isEmpty(topicoForum.getPessoaCadastro())) {
            topicoForum.setPessoaCadastro(new Pessoa(user.getId()));
        }

        return this.topicoForumService.salvar(user.getId(), topicoForum).getId();
    }

    @DeleteMapping(path = "/{id}")
    public void deletar(UsuarioForum user, @PathVariable("id") Long idTopico) throws BusinessRuleException {
        this.topicoForumService.deletar(user.getId(), idTopico);
    }
}
