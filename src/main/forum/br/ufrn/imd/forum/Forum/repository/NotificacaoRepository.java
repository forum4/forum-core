package br.ufrn.imd.forum.Forum.repository;

import br.ufrn.imd.forum.Forum.domain.Duvida;
import br.ufrn.imd.forum.Forum.domain.Notificacao;
import br.ufrn.imd.forum.Forum.domain.Pessoa;
import br.ufrn.imd.forum.Forum.domain.TopicoForum;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface NotificacaoRepository extends JpaRepository<Notificacao, Long> {
    List<Notificacao> findAllByTopicoForum(TopicoForum topicoForum);

    List<Notificacao> findAllByDuvida(Duvida duvida);

    Optional<Notificacao> findByPessoaAndTopicoForum(Pessoa pessoa, TopicoForum topicoForum);

    Optional<Notificacao> findByPessoaAndDuvida(Pessoa pessoa, Duvida duvida);
}
