package br.ufrn.imd.forum.Forum.dto;

import br.ufrn.imd.forum.Forum.enums.TipoDenuncia;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class DenunciaDTO {
    private Long id;
    private String descricao;
    private Date dataCriado;
    private TipoDTO<Long> pessoaCadastro;
    private TipoDenuncia tipoDenuncia;
    private Long idTipo;
}
