package br.ufrn.imd.forum.Forum.service;

import br.ufrn.imd.forum.Forum.core.auth.validators.SenhaValidatorUtils;
import br.ufrn.imd.forum.Forum.core.utils.PageResponse;
import br.ufrn.imd.forum.Forum.core.utils.QueryFilter;
import br.ufrn.imd.forum.Forum.core.utils.ValidatorUtil;
import br.ufrn.imd.forum.Forum.domain.Pessoa;
import br.ufrn.imd.forum.Forum.enums.TipoPapel;
import br.ufrn.imd.forum.Forum.exception.BusinessRuleException;
import br.ufrn.imd.forum.Forum.repository.PessoaRepository;
import br.ufrn.imd.forum.Forum.service.interfaces.ServiceInterfaceSimple;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class PessoaService implements ServiceInterfaceSimple<Pessoa, Long> {
    private final PessoaRepository pessoaRepository;

    @Override
    @Transactional
    public Pessoa salvar(Pessoa entidade) throws BusinessRuleException {
        this.validarPessoa(entidade);
        Pessoa pessoaBd = new Pessoa();

        if (ValidatorUtil.isEmpty(entidade.getId())) {
            // Então está cadastrando
            pessoaBd.setSenha(SenhaValidatorUtils.encryptPassword(entidade.getSenha()));
            pessoaBd.setPapel(TipoPapel.COMUM);
        } else {
            pessoaBd = this.buscar(entidade.getId());
        }

        BeanUtils.copyProperties(entidade, pessoaBd, Pessoa.ignoreProperties);

        pessoaBd.setEmail(pessoaBd.getEmail().strip().toLowerCase());
        pessoaBd.setUsuario(pessoaBd.getUsuario().strip().toLowerCase());

        this.validarPessoa(pessoaBd);

        return this.pessoaRepository.save(pessoaBd);
    }

    /**
     * Método para validação de uma entidade Pessoa
     *
     * @param pessoa - Entidade a ser validada
     * @throws BusinessRuleException - Se não seguir as regras
     */
    private void validarPessoa(Pessoa pessoa) throws BusinessRuleException {
        if (ValidatorUtil.isEmpty(pessoa)) {
            throw new BusinessRuleException("Entidade não pode ser nula");
        }

        if (ValidatorUtil.isEmpty(pessoa.getNome())) {
            throw new BusinessRuleException("Nome não pode ser vazio");
        }

        if (ValidatorUtil.isEmpty(pessoa.getId()) && ValidatorUtil.isEmpty(pessoa.getSenha())) {
            throw new BusinessRuleException("Senha não pode ser vazia");
        }

        if (ValidatorUtil.isEmpty(pessoa.getEmail())) {
            throw new BusinessRuleException("Email não pode ser vazio");
        } else if (!ValidatorUtil.isEmailValid(pessoa.getEmail().toLowerCase())) {
            throw new BusinessRuleException("Email com formato não suportado");
        }

        if (ValidatorUtil.isEmpty(pessoa.getUsuario())) {
            throw new BusinessRuleException("Usuário não pode ser vazio");
        } else if (!ValidatorUtil.isUsuarioValid(pessoa.getUsuario().toLowerCase().strip())) {
            throw new BusinessRuleException("Usuário com formato não suportado");
        }

        if (ValidatorUtil.isEmpty(pessoa.getDataNascimento())) {
            throw new BusinessRuleException("Data de nascimento não pode ser vazia");
        }

        Pessoa pessoaBD = this.pessoaRepository.findByEmailOrUsuario(pessoa.getEmail(), pessoa.getUsuario()).orElse(null);
        if (ValidatorUtil.isNotEmpty(pessoaBD) && !pessoaBD.getId().equals(pessoa.getId())) {
            throw new BusinessRuleException("Email ou Usuário já existentes");
        }
    }

    @Override
    public PageResponse<Pessoa> buscarTodos(QueryFilter filtro) {
        return new PageResponse<>();
    }

    public boolean existsPessoaPorUsername(Long idPessoa, String usuario) {
        Pessoa pessoa = this.pessoaRepository.findByUsuario(usuario).orElse(null);
        return ValidatorUtil.isNotEmpty(pessoa) && ValidatorUtil.isNotEmpty(idPessoa) && pessoa.getId().equals(idPessoa);
    }

    public boolean existsPessoaPorEmail(Long idPessoa, String email) {
        Pessoa pessoa = this.pessoaRepository.findByEmail(email).orElse(null);
        return ValidatorUtil.isNotEmpty(pessoa) && ValidatorUtil.isNotEmpty(idPessoa) && pessoa.getId().equals(idPessoa);
    }

    @Override
    public Pessoa buscar(Long id) {
        return this.pessoaRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Pessoa não encontrada"));
    }

    @Override
    @Transactional
    public void deletar(Long id) {
        Pessoa pessoa = this.buscar(id);
        pessoa.setAtivo(false);
        this.pessoaRepository.save(pessoa);
    }
}
