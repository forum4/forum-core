package br.ufrn.imd.forum.Forum.mapper;

import br.ufrn.imd.forum.Forum.domain.Denuncia;
import br.ufrn.imd.forum.Forum.dto.DenunciaDTO;
import br.ufrn.imd.forum.Forum.dto.TipoDTO;
import org.springframework.beans.BeanUtils;

public class DenunciaMapper {
    public static Denuncia converter(DenunciaDTO denunciaDTO) {
        Denuncia denuncia = new Denuncia();
        BeanUtils.copyProperties(denunciaDTO, denuncia);

        denuncia.setId(denunciaDTO.getId());

        return denuncia;
    }

    public static DenunciaDTO converter(Denuncia denuncia) {
        DenunciaDTO denunciaDTO = new DenunciaDTO();
        BeanUtils.copyProperties(denuncia, denunciaDTO);

        denunciaDTO.setId(denuncia.getId());

        denunciaDTO.setPessoaCadastro(new TipoDTO(denuncia.getPessoaCadastro().getId(), denuncia.getPessoaCadastro().getNome()));

        return denunciaDTO;
    }
}
