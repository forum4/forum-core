package br.ufrn.imd.forum.Forum.rest;

import br.ufrn.imd.forum.Forum.core.utils.PageResponse;
import br.ufrn.imd.forum.Forum.core.utils.QueryFilter;
import br.ufrn.imd.forum.Forum.domain.Tag;
import br.ufrn.imd.forum.Forum.dto.TagDTO;
import br.ufrn.imd.forum.Forum.exception.BusinessRuleException;
import br.ufrn.imd.forum.Forum.mapper.TagMapper;
import br.ufrn.imd.forum.Forum.service.TagService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/tag")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", maxAge = 3600)
public class TagRestController {
    private final TagService tagService;

    @GetMapping
    public PageResponse<TagDTO> buscarTodos(
            @RequestParam(value = "search", required = false) String search,
            @RequestParam(value = "id_related", required = false) Long idRelacionado,
            @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @RequestParam(value = "pageSize", required = false, defaultValue = "20") int pageSize
    ) {
        QueryFilter filtro = new QueryFilter(search, idRelacionado, page, pageSize);
        PageResponse<Tag> tags = this.tagService.buscarTodos(filtro);
        PageResponse<TagDTO> response = new PageResponse<>(tags);

        response.setConteudo(
                tags.getConteudo().stream().map(TagMapper::converter).collect(Collectors.toList())
        );

        return response;
    }

    @GetMapping(path = "/{id}")
    public TagDTO buscarUm(@PathVariable("id") Long idTag) {
        return TagMapper.converter(this.tagService.buscar(idTag));
    }

    @PostMapping
    public Long cadastrar(@RequestBody TagDTO tagDTO) throws BusinessRuleException {
        Tag tag = TagMapper.converter(tagDTO);
        Tag tagBd = this.tagService.cadastrar(tag);

        return tagBd.getId();
    }

    @PutMapping
    public Long editar(@RequestBody TagDTO tagDTO) throws BusinessRuleException {
        Tag tag = TagMapper.converter(tagDTO);
        return this.tagService.editar(tag).getId();
    }

    @DeleteMapping(path = "/{id}")
    public void deletar(@PathVariable("id") Long idTag) throws BusinessRuleException {
        this.tagService.deletar(idTag);
    }
}
