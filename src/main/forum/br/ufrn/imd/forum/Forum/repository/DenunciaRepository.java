package br.ufrn.imd.forum.Forum.repository;

import br.ufrn.imd.forum.Forum.domain.Denuncia;
import br.ufrn.imd.forum.Forum.domain.Pessoa;
import br.ufrn.imd.forum.Forum.enums.TipoDenuncia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DenunciaRepository extends JpaRepository<Denuncia, Long> {
    @Query(value = "SELECT DISTINCT den.* FROM denuncias den WHERE den.id_pessoa_cadastro = :search AND den.ativo = TRUE ORDER BY den.data_criado ASC LIMIT :pageSize OFFSET :skipRows", nativeQuery = true)
    List<Denuncia> findAllByUsuarioPageable(@Param("search") Long search, @Param("pageSize") int pageSize, @Param("skipRows") int skipRows);

    @Query(value = "SELECT COUNT(DISTINCT den.id) FROM denuncias den WHERE den.id_pessoa_cadastro = :search AND den.ativo = TRUE", nativeQuery = true)
    Integer countDistinctByUsuarioPageable(@Param("search") Long search);

    @Query(value = "SELECT COUNT(DISTINCT den.id) FROM Denuncia den WHERE den.tipoDenuncia = :search AND den.idTipo = :idTipo AND den.ativo = TRUE")
    Integer countDistinctDenunciaByTipoAndId(@Param("search") TipoDenuncia search, @Param("idTipo") Long idTipo);

    @Query(value = "SELECT COUNT(DISTINCT den.id) FROM Denuncia den WHERE den.tipoDenuncia = :search AND den.idTipo = :idTipo AND den.pessoaCadastro = :pessoa AND den.ativo = TRUE")
    Integer countDistinctDenunciaByTipoAndPessoaCadastro(@Param("search") TipoDenuncia search, @Param("idTipo") Long idTipo, @Param("pessoa")Pessoa pessoa);
}
