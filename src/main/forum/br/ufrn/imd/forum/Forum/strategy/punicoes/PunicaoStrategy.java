package br.ufrn.imd.forum.Forum.strategy.punicoes;

import br.ufrn.imd.forum.Forum.domain.Duvida;
import br.ufrn.imd.forum.Forum.domain.Resposta;
import br.ufrn.imd.forum.Forum.domain.TopicoForum;

public interface PunicaoStrategy {
    void punir(TopicoForum topicoForum);

    void punir(Duvida duvida);

    void punir(Resposta reposta);
}
