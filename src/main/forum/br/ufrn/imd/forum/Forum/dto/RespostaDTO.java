package br.ufrn.imd.forum.Forum.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class RespostaDTO {
    private Long id;
    private String resposta;
    private Date dataCriado;
    private Date dataUltimaEdicao;
    private int likes;
    private int dislikes;
    private TipoDTO<Long> pessoaCadastro;
    private TipoDTO<Long> pessoaUltimaEdicao;
    private TipoDTO<Long> duvida;
}
