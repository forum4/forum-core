package br.ufrn.imd.forum.Forum.service.interfaces;

import br.ufrn.imd.forum.Forum.exception.BusinessRuleException;

public interface ServiceInterfaceSimple<T, ID> extends ServiceInterface<T, ID> {
    T salvar(T entidade) throws BusinessRuleException;

    void deletar(ID id);
}
