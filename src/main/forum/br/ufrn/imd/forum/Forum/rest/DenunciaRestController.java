package br.ufrn.imd.forum.Forum.rest;

import br.ufrn.imd.forum.Forum.core.auth.users.UsuarioForum;
import br.ufrn.imd.forum.Forum.core.utils.PageResponse;
import br.ufrn.imd.forum.Forum.core.utils.QueryFilter;
import br.ufrn.imd.forum.Forum.domain.Denuncia;
import br.ufrn.imd.forum.Forum.dto.DenunciaDTO;
import br.ufrn.imd.forum.Forum.exception.BusinessRuleException;
import br.ufrn.imd.forum.Forum.mapper.DenunciaMapper;
import br.ufrn.imd.forum.Forum.service.DenunciaService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/denuncia")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", maxAge = 3600)
public class DenunciaRestController {
    private final DenunciaService denunciaService;

    @GetMapping
    public PageResponse<DenunciaDTO> buscarTodos(
            @RequestParam(value = "search", required = false) String search,
            @RequestParam(value = "id_related", required = false) Long idRelacionado,
            @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @RequestParam(value = "pageSize", required = false, defaultValue = "20") int pageSize
    ) {
        QueryFilter filtro = new QueryFilter(search, idRelacionado, page, pageSize);
        PageResponse<Denuncia> denuncias = this.denunciaService.buscarTodos(filtro);
        PageResponse<DenunciaDTO> response = new PageResponse<>(denuncias);

        response.setConteudo(
                denuncias.getConteudo().stream().map(DenunciaMapper::converter).collect(Collectors.toList())
        );

        return response;
    }

    @GetMapping(path = "/{id}")
    public DenunciaDTO buscarUm(@PathVariable("id") Long idDenuncia) {
        return DenunciaMapper.converter(this.denunciaService.buscar(idDenuncia));
    }

    @PostMapping
    public Long cadastrar(UsuarioForum user, @RequestBody DenunciaDTO denunciaDTO) throws BusinessRuleException {
        Denuncia denuncia = DenunciaMapper.converter(denunciaDTO);
        return this.denunciaService.cadastrar(user.getId(), denuncia).getId();
    }

    @PutMapping
    public Long editar(UsuarioForum user, @RequestBody DenunciaDTO denunciaDTO) throws BusinessRuleException {
        Denuncia denuncia = DenunciaMapper.converter(denunciaDTO);
        return this.denunciaService.editar(user.getId(), denuncia).getId();
    }
}
