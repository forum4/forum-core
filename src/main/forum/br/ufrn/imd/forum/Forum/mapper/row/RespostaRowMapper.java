package br.ufrn.imd.forum.Forum.mapper.row;

import br.ufrn.imd.forum.Forum.domain.Duvida;
import br.ufrn.imd.forum.Forum.domain.Pessoa;
import br.ufrn.imd.forum.Forum.domain.Resposta;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RespostaRowMapper implements RowMapper<Resposta> {
    @Override
    public Resposta mapRow(ResultSet rs, int rowNum) throws SQLException {
        Resposta resposta = new Resposta();

        resposta.setId(rs.getLong("id"));
        resposta.setResposta(rs.getString("resposta"));
        resposta.setAtivo(rs.getBoolean("ativo"));
        resposta.setDataCriado(rs.getTimestamp("data_criado"));
        resposta.setDataUltimaEdicao(rs.getTimestamp("data_ultima_edicao"));

        resposta.setLikes(rs.getInt("likes"));
        resposta.setDislikes(rs.getInt("dislikes"));

        resposta.setPessoaCadastro(new Pessoa(rs.getLong("id_pessoa_cadastro"), rs.getString("nome_pessoa_cadastro")));
        resposta.setPessoaUltimaEdicao(new Pessoa(rs.getLong("id_pessoa_update"), rs.getString("nome_pessoa_update")));

        resposta.setDuvida(new Duvida(rs.getLong("id_duvida"), rs.getString("titulo_duvida")));

        return resposta;
    }
}
