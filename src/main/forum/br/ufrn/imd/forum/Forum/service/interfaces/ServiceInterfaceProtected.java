package br.ufrn.imd.forum.Forum.service.interfaces;

import br.ufrn.imd.forum.Forum.exception.BusinessRuleException;

public interface ServiceInterfaceProtected<T, ID> extends ServiceInterface<T, ID> {
    T salvar(Long idUsuario, T entidade) throws BusinessRuleException;

    void deletar(Long idUsuario, ID id) throws BusinessRuleException;
}
