package br.ufrn.imd.forum.Forum.strategy.punicoes;

import br.ufrn.imd.forum.Forum.domain.Duvida;
import br.ufrn.imd.forum.Forum.domain.Resposta;
import br.ufrn.imd.forum.Forum.domain.TopicoForum;
import br.ufrn.imd.forum.Forum.enums.TipoDenuncia;
import br.ufrn.imd.forum.Forum.repository.DenunciaRepository;
import br.ufrn.imd.forum.Forum.service.DuvidaService;
import br.ufrn.imd.forum.Forum.service.RespostaService;
import br.ufrn.imd.forum.Forum.service.TopicoForumService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class PunicaoItem implements PunicaoStrategy {

    private final DenunciaRepository denunciaRepository;
    private final TopicoForumService topicoForumService;
    private final DuvidaService duvidaService;
    private final RespostaService respostaService;

    private final static int valorDenuncia = 5;

    @Override
    public void punir(TopicoForum topicoForum) {
        if (this.denunciaRepository.countDistinctDenunciaByTipoAndId(TipoDenuncia.TOPICO_FORUM,
                topicoForum.getId()) >= valorDenuncia) {
            this.topicoForumService.desativarTopico(topicoForum);
        }
    }

    @Override
    public void punir(Duvida duvida) {
        if (this.denunciaRepository.countDistinctDenunciaByTipoAndId(TipoDenuncia.DUVIDA,
                duvida.getId()) >= valorDenuncia) {
            this.duvidaService.desativarDuvida(duvida);
        }
    }

    @Override
    public void punir(Resposta resposta) {
        if (this.denunciaRepository.countDistinctDenunciaByTipoAndId(TipoDenuncia.RESPOSTA,
                resposta.getId()) >= valorDenuncia) {
            this.respostaService.desativarResposta(resposta);
        }
    }
}
