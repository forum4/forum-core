package br.ufrn.imd.forum.Forum.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "resposta")
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = {"id"})
public class Resposta {
    @Id
    @GeneratedValue
    private Long id;

    private String resposta;

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataCriado = new Date();

    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataUltimaEdicao = new Date();

    @Version
    private Long versao = 0L;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_pessoa_cadastro")
    private Pessoa pessoaCadastro;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_pessoa_ultima_edicao")
    private Pessoa pessoaUltimaEdicao;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_duvida")
    private Duvida duvida;

    private boolean ativo = true;

    @Transient
    private int likes;

    @Transient
    private int dislikes;

    @Transient
    public static final String[] ignoreProperties = {
            "id",
            "dataCriado",
            "dataUltimaEdicao",
            "versao",
            "pessoaCadastro",
            "pessoaUltimaEdicao",
            "duvida",
            "ignoreProperties"
    };
}
