package br.ufrn.imd.forum.Forum.adapter;

import br.ufrn.imd.forum.Forum.domain.Duvida;
import br.ufrn.imd.forum.Forum.domain.Pessoa;
import br.ufrn.imd.forum.Forum.domain.Resposta;

import javax.mail.MessagingException;
import java.util.List;

public interface NotificacaoProtocol {
    void notificar(Duvida duvida, List<Pessoa> destinatarios) throws MessagingException;

    void notificar(Resposta resposta, List<Pessoa> destinatarios) throws MessagingException;
}
