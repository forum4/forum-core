package br.ufrn.imd.forum.Forum.repository;

import br.ufrn.imd.forum.Forum.domain.Pessoa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface PessoaRepository extends JpaRepository<Pessoa, Long> {
    @Query("SELECT p FROM Pessoa p WHERE LOWER(p.email) = LOWER(:email) OR LOWER(p.usuario) = LOWER(:usuario)")
    Optional<Pessoa> findByEmailOrUsuario(@Param("email") String email, @Param("usuario") String usuario);

    default Optional<Pessoa> findByEmailOrUsuario(String search) {
        return findByEmailOrUsuario(search, search);
    }

    Optional<Pessoa> findByEmail(String email);

    Optional<Pessoa> findByUsuario(String usuario);
}
