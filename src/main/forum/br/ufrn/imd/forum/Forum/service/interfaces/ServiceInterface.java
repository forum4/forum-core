package br.ufrn.imd.forum.Forum.service.interfaces;

import br.ufrn.imd.forum.Forum.core.utils.PageResponse;
import br.ufrn.imd.forum.Forum.core.utils.QueryFilter;

public interface ServiceInterface<T, ID> {
    PageResponse<T> buscarTodos(QueryFilter filtro);

    T buscar(ID id);
}
