package br.ufrn.imd.forum.Forum.mapper;

import br.ufrn.imd.forum.Forum.core.utils.ValidatorUtil;
import br.ufrn.imd.forum.Forum.domain.Duvida;
import br.ufrn.imd.forum.Forum.domain.Tag;
import br.ufrn.imd.forum.Forum.domain.TopicoForum;
import br.ufrn.imd.forum.Forum.dto.DuvidaDTO;
import br.ufrn.imd.forum.Forum.dto.TipoDTO;
import org.springframework.beans.BeanUtils;

import java.util.stream.Collectors;

public class DuvidaMapper {

    public static Duvida converter(DuvidaDTO duvidaDTO) {
        Duvida duvida = new Duvida();
        BeanUtils.copyProperties(duvidaDTO, duvida);
        duvida.setId(duvidaDTO.getId());
        duvida.setTitulo(duvidaDTO.getNome());
        duvida.setTopicoForum(new TopicoForum());
        duvida.getTopicoForum().setId(duvidaDTO.getTopicoForum().getId());
        if (ValidatorUtil.isNotEmpty(duvidaDTO.getRespostaCorreta())) {
            duvida.getRespostaCorreta().setId(duvidaDTO.getRespostaCorreta().getId());
        }

        if (!duvidaDTO.getTags().isEmpty()) {
            duvida.setTags(duvidaDTO.getTags().stream().map(item -> {
                Tag tag = new Tag();
                tag.setId(item.getId());
                tag.setDescricao(item.getNome());
                return tag;
            }).collect(Collectors.toList()));
        }

        return duvida;
    }

    public static DuvidaDTO converter(Duvida duvida) {
        DuvidaDTO duvidaDTO = new DuvidaDTO();
        BeanUtils.copyProperties(duvida, duvidaDTO);
        duvidaDTO.setNome(duvida.getTitulo());

        duvidaDTO.setPessoaCadastro(
                new TipoDTO<>(duvida.getPessoaCadastro().getId(), duvida.getPessoaCadastro().getNome())
        );
        duvidaDTO.setPessoaUltimaEdicao(
                new TipoDTO<>(duvida.getPessoaUltimaEdicao().getId(), duvida.getPessoaUltimaEdicao().getNome())
        );
        duvidaDTO.setTopicoForum(
                new TipoDTO<>(duvida.getTopicoForum().getId(), duvida.getTopicoForum().getTitulo())
        );
        if (ValidatorUtil.isNotEmpty(duvida.getRespostaCorreta())) {
            duvidaDTO.setRespostaCorreta(
                    new TipoDTO<>(duvida.getRespostaCorreta().getId(), duvida.getRespostaCorreta().getResposta())
            );
        }

        if (ValidatorUtil.isNotEmpty(duvida.getTags())) {
            duvidaDTO.setTags(duvida.getTags().stream().map(tag ->
                    new TipoDTO<>(tag.getId(), tag.getDescricao())
            ).collect(Collectors.toList()));
        }

        return duvidaDTO;
    }

}
