package br.ufrn.imd.forum.Forum.service;

import br.ufrn.imd.forum.Forum.core.utils.PageResponse;
import br.ufrn.imd.forum.Forum.core.utils.QueryFilter;
import br.ufrn.imd.forum.Forum.core.utils.ValidatorUtil;
import br.ufrn.imd.forum.Forum.domain.Tag;
import br.ufrn.imd.forum.Forum.exception.BusinessRuleException;
import br.ufrn.imd.forum.Forum.repository.TagRepository;
import br.ufrn.imd.forum.Forum.service.interfaces.ServiceInterfaceSimple;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TagService implements ServiceInterfaceSimple<Tag, Long> {
    private final TagRepository tagRepository;

    @Override
    public PageResponse<Tag> buscarTodos(QueryFilter filtro) {
        PageResponse<Tag> response = new PageResponse<>();
        int skipRows = (filtro.getPage() - 1) * filtro.getPageSize();

        List<Tag> tags = tagRepository.findAllPageable(filtro.getPageSize(), skipRows);
        Integer total = this.tagRepository.countDistinctAllByPageable(filtro.getPageSize(), skipRows);

        if (ValidatorUtil.isEmpty(total)) {
            total = 0;
        }

        response.setConteudo(tags);
        response.setPaginaAtual(filtro.getPage());
        response.setTamanhoPagina(filtro.getPageSize());
        response.setTotalElementos(total.longValue());
        response.setTotalPaginas((long) (total / filtro.getPageSize()));

        return response;

    }

    @Override
    public Tag buscar(Long id) {
        return this.tagRepository.findById(id).orElseThrow(() ->
                new ResourceNotFoundException("Tag não encontrada!"));
    }

    @Override
    public Tag salvar(Tag entidade) throws BusinessRuleException {
        this.validarTag(entidade);
        return this.tagRepository.save(entidade);
    }

    @Override
    public void deletar(Long id) {
        Tag tag = this.buscar(id);
        tag.setAtivo(false);

        this.tagRepository.save(tag);
    }

    @Transactional
    public Tag cadastrar(Tag tag) throws BusinessRuleException {

        return this.salvar(tag);
    }

    @Transactional
    public Tag editar(Tag tag) throws BusinessRuleException {
        Tag tagBd = this.buscar(tag.getId());
        BeanUtils.copyProperties(tag, tagBd, Tag.ignoreProperties);

        return this.salvar(tagBd);
    }

    private void validarTag(Tag tag) throws BusinessRuleException {
        if (ValidatorUtil.isEmpty(tag.getDescricao())) {
            throw new BusinessRuleException("Descrição não pode ser vazia");
        }
    }
}
