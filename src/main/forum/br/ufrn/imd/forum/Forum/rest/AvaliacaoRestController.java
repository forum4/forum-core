package br.ufrn.imd.forum.Forum.rest;

import br.ufrn.imd.forum.Forum.core.auth.users.UsuarioForum;
import br.ufrn.imd.forum.Forum.enums.TipoAvaliacao;
import br.ufrn.imd.forum.Forum.exception.BusinessRuleException;
import br.ufrn.imd.forum.Forum.service.AvaliacaoService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/avaliacao")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", maxAge = 3600)
public class AvaliacaoRestController {
    private final AvaliacaoService avaliacaoService;

    @PostMapping
    public Long salvar(
            UsuarioForum user,
            @RequestParam("id_resposta") Long idResposta,
            @RequestParam("tipo_avaliacao") TipoAvaliacao tipoAvaliacao
    ) throws BusinessRuleException {
        return this.avaliacaoService.salvar(user.getId(), idResposta, tipoAvaliacao).getId();
    }

    public void deletar(
            UsuarioForum user,
            @RequestParam("id_resposta") Long idResposta
    ) throws BusinessRuleException {
        this.avaliacaoService.deletar(user.getId(), idResposta);
    }
}
