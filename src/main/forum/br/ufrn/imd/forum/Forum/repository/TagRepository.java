package br.ufrn.imd.forum.Forum.repository;

import br.ufrn.imd.forum.Forum.domain.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TagRepository extends JpaRepository<Tag, Long> {
    @Query(value = "SELECT DISTINCT tg.* FROM tags tg WHERE tg.ativo = TRUE LIMIT :pageSize OFFSET :skipRows", nativeQuery = true)
    List<Tag> findAllPageable( @Param("pageSize") int pageSize, @Param("skipRows") int skipRows);

    @Query(value = "SELECT COUNT(DISTINCT tg.*) FROM tags tg WHERE tg.ativo = TRUE LIMIT :pageSize OFFSET :skipRows", nativeQuery = true)
    Integer countDistinctAllByPageable( @Param("pageSize") int pageSize, @Param("skipRows") int skipRows);
}
