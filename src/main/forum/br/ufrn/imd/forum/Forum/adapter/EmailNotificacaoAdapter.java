package br.ufrn.imd.forum.Forum.adapter;

import br.ufrn.imd.forum.Forum.domain.Duvida;
import br.ufrn.imd.forum.Forum.domain.Pessoa;
import br.ufrn.imd.forum.Forum.domain.Resposta;
import br.ufrn.imd.forum.Forum.domain.TopicoForum;
import br.ufrn.imd.forum.Forum.factory.FabricaNotificacao;
import br.ufrn.imd.forum.Forum.factory.FabricaNotificacaoPadrao;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

public class EmailNotificacaoAdapter implements NotificacaoProtocol {
    private final String email = "notificacao.forum@gmail.com";
    private final String senha = "not1ficac@of0rum";
    private final FabricaNotificacao fabricaNotificacao;

    public EmailNotificacaoAdapter() {
        this.fabricaNotificacao = new FabricaNotificacaoPadrao();
    }

    private Properties getMailProperties() {
        Properties props = new Properties();
        // Parâmetros de conexão com servidor Gmail
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        return props;
    }

    private Authenticator getAuthenticator() {
        return new Authenticator() {

            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(email, senha);
            }
        };
    }

    private Session getMailSession() {
        return Session.getDefaultInstance(this.getMailProperties(), this.getAuthenticator());
    }

    private Address[] getAddressList(List<Pessoa> destinatarios) throws AddressException {
        return InternetAddress.parse(
                new HashSet<>(destinatarios).stream().map(Pessoa::getEmail).collect(Collectors.joining(", "))
        );
    }

    private Message getMessage(List<Pessoa> destinatarios) throws MessagingException {
        Session session = getMailSession();

        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(email));
        //Remetente

        Address[] toUser = getAddressList(destinatarios);

        message.setRecipients(Message.RecipientType.TO, toUser);

        return message;
    }

    private String createEmailSubject(TopicoForum topicoForum) {
        return "Nova dúvida cadastrada para o tópico de fórum: " + topicoForum.getTitulo();
    }

    private String createEmailSubject(Duvida duvida) {
        return "Nova resposta cadastrada para dúvida: " + duvida.getTitulo();
    }

    @Override
    public void notificar(Duvida duvida, List<Pessoa> destinatarios) throws MessagingException {
        Message message = this.getMessage(destinatarios);
        // Assunto
        message.setSubject(this.createEmailSubject(duvida.getTopicoForum()));
        // Conteúdo
        message.setContent(this.fabricaNotificacao.buildTextoNotificacao(duvida), "text/html;charset=UTF-8");
        // Método para enviar a mensagem criada
        this.sendMessage(message);
    }

    @Override
    public void notificar(Resposta resposta, List<Pessoa> destinatarios) throws MessagingException {
        Message message = this.getMessage(destinatarios);
        // Assunto
        message.setSubject(this.createEmailSubject(resposta.getDuvida()));
        // Conteúdo
        message.setContent(this.fabricaNotificacao.buildTextoNotificacao(resposta), "text/html;charset=UTF-8");
        // Método para enviar a mensagem criada
        this.sendMessage(message);
    }

    private void sendMessage(Message message) throws MessagingException {
        Transport.send(message);
    }
}
