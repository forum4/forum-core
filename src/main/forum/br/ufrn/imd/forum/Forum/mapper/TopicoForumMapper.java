package br.ufrn.imd.forum.Forum.mapper;

import br.ufrn.imd.forum.Forum.core.utils.ValidatorUtil;
import br.ufrn.imd.forum.Forum.domain.Tag;
import br.ufrn.imd.forum.Forum.domain.TopicoForum;
import br.ufrn.imd.forum.Forum.dto.TipoDTO;
import br.ufrn.imd.forum.Forum.dto.TopicoForumDTO;
import org.springframework.beans.BeanUtils;

import java.util.stream.Collectors;

public class TopicoForumMapper {
    public static TopicoForum converter(TopicoForumDTO topico) {
        TopicoForum topicoForum = new TopicoForum();
        BeanUtils.copyProperties(topico, topicoForum);
        topicoForum.setId(topico.getId());
        topicoForum.setTitulo(topico.getNome());

        if (!topico.getTags().isEmpty()) {
            topicoForum.setTags(topico.getTags().stream().map(item -> {
                Tag tag = new Tag();
                tag.setId(item.getId());
                tag.setDescricao(item.getNome());
                return tag;
            }).collect(Collectors.toList()));
        }

        return topicoForum;
    }

    public static TopicoForumDTO converter(TopicoForum topicoForum) {
        TopicoForumDTO topico = new TopicoForumDTO();
        BeanUtils.copyProperties(topicoForum, topico);
        topico.setNome(topicoForum.getTitulo());
        topico.setPessoaCadastro(
                new TipoDTO<>(topicoForum.getPessoaCadastro().getId(), topicoForum.getPessoaCadastro().getNome())
        );

        if (ValidatorUtil.isNotEmpty(topicoForum.getTags())) {
            topico.setTags(topicoForum.getTags().stream().map(tag ->
                    new TipoDTO<>(tag.getId(), tag.getDescricao())
            ).collect(Collectors.toList()));
        }


        return topico;
    }
}
