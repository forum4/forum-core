package br.ufrn.imd.forum.Forum.service;

import br.ufrn.imd.forum.Forum.adapter.EmailNotificacaoAdapter;
import br.ufrn.imd.forum.Forum.adapter.NotificacaoProtocol;
import br.ufrn.imd.forum.Forum.core.utils.ValidatorUtil;
import br.ufrn.imd.forum.Forum.domain.*;
import br.ufrn.imd.forum.Forum.exception.BusinessRuleException;
import br.ufrn.imd.forum.Forum.repository.NotificacaoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class NotificacaoService {
    private final NotificacaoRepository notificacaoRepository;

    public Notificacao cadastrar(Pessoa pessoa, TopicoForum topicoForum) throws BusinessRuleException {
        Notificacao notificacao = new Notificacao();
        notificacao.setPessoa(pessoa);
        notificacao.setTopicoForum(topicoForum);

        return this.salvar(notificacao);
    }

    public Notificacao cadastrar(Pessoa pessoa, Duvida duvida) throws BusinessRuleException {
        Notificacao notificacao = new Notificacao();
        notificacao.setPessoa(pessoa);
        notificacao.setDuvida(duvida);

        return this.salvar(notificacao);
    }

    public Notificacao salvar(Notificacao notificacao) throws BusinessRuleException {
        this.validar(notificacao);

        return this.notificacaoRepository.save(notificacao);
    }

    private void validar(Notificacao notificacao) throws BusinessRuleException {
        List<String> mensagens = new ArrayList<>();

        if (ValidatorUtil.isEmpty(notificacao)) {
            mensagens.add("Notificação não pode ser vazia");
            throw new BusinessRuleException(mensagens);
        }

        if (ValidatorUtil.isEmpty(notificacao.getPessoa())) {
            mensagens.add("Notificação não vinculada a um usuário");
        }

        if (ValidatorUtil.isEmpty(notificacao.getTopicoForum()) && ValidatorUtil.isEmpty(notificacao.getDuvida())) {
            mensagens.add("Notificação não vinculada a um tópico de fórum ou dúvida");
        }

        Notificacao notificacaoBd = null;
        // Notificação de pessoa para tópico de fórum
        if (ValidatorUtil.isNotEmpty(notificacao.getTopicoForum())) {
            notificacaoBd = this.notificacaoRepository.findByPessoaAndTopicoForum(notificacao.getPessoa(), notificacao.getTopicoForum()).orElse(null);
        }

        // Notificação de pessoa para tópico de fórum
        if (ValidatorUtil.isNotEmpty(notificacao.getDuvida())) {
            notificacaoBd = this.notificacaoRepository.findByPessoaAndDuvida(notificacao.getPessoa(), notificacao.getDuvida()).orElse(null);
        }

        if (ValidatorUtil.isNotEmpty(notificacaoBd)) {
            mensagens.add("Notificação já cadastrada");
        }

        if (ValidatorUtil.isNotEmpty(mensagens)) {
            throw new BusinessRuleException(mensagens);
        }
    }

    /**
     * Notificar usuários que se inscreveram no tópico de fórum
     *
     * @param duvida - Duvida cadastrada em um tópico de fórum
     * @throws MessagingException Se a mensagem falhar
     */
    public void notificar(Duvida duvida) throws MessagingException {
        List<Notificacao> notificacoes = this.notificacaoRepository.findAllByTopicoForum(duvida.getTopicoForum());

        if (ValidatorUtil.isNotEmpty(notificacoes)) {
            NotificacaoProtocol notificacaoProtocol = new EmailNotificacaoAdapter();
            notificacaoProtocol.notificar(duvida, notificacoes.stream().map(Notificacao::getPessoa).collect(Collectors.toList()));
        }
    }

    /**
     * Notificar usuários que se inscrevefram no tópico de fórum
     *
     * @param resposta - Resposta cadastrada em uma dúvida
     * @throws MessagingException Se a mensagem falhar
     */
    public void notificar(Resposta resposta) throws MessagingException {
        List<Notificacao> notificacoes = this.notificacaoRepository.findAllByDuvida(resposta.getDuvida());

        if (ValidatorUtil.isNotEmpty(notificacoes)) {
            NotificacaoProtocol notificacaoProtocol = new EmailNotificacaoAdapter();
            notificacaoProtocol.notificar(resposta, notificacoes.stream().map(Notificacao::getPessoa).collect(Collectors.toList()));
        }
    }
}
