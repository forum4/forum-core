package br.ufrn.imd.forum.Forum.repository;

import br.ufrn.imd.forum.Forum.domain.Resposta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RespostaRepository extends JpaRepository<Resposta, Long>, CustomRespostaRepository {

    @Query(value = "SELECT COUNT(DISTINCT res.id) FROM resposta res WHERE res.id_duvida = :search AND res.ativo = TRUE", nativeQuery = true)
    Integer countDistinctByDuvidaPageable(@Param("search") Long search);
}
