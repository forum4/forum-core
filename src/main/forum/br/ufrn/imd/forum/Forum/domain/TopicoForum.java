package br.ufrn.imd.forum.Forum.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "topico_forum")
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = {"id"})
public class TopicoForum {
    @Id
    @GeneratedValue
    private Long id;

    private String titulo;

    private String descricao;

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataCriado = new Date();

    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataUltimaEdicao = new Date();

    @Version
    private Long versao = 0L;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_pessoa_cadastro")
    private Pessoa pessoaCadastro;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_pessoa_ultima_edicao")
    private Pessoa pessoaUltimaEdicao;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "topicoForum")
    private List<Duvida> duvidas;

    @ManyToMany
    @JoinTable(name="topico_has_tags", joinColumns=
            {@JoinColumn(name="topico_forum_id")}, inverseJoinColumns=
            {@JoinColumn(name="tags_id")})
    private List<Tag> tags;

    private boolean ativo = true;

    @Transient
    public static final String[] ignoreProperties = {"id", "dataCriado", "dataUltimaEdicao", "versao", "pessoaCadastro", "pessoaUltimaEdicao", "duvidas", "ignoreProperties"};
}
