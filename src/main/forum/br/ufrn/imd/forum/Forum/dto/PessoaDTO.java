package br.ufrn.imd.forum.Forum.dto;

import br.ufrn.imd.forum.Forum.domain.Pessoa;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class PessoaDTO extends TipoDTO<Long> {
    private String usuario;
    private Date dataNascimento;
    private String email;

    public PessoaDTO(Pessoa pessoa) {
        super(pessoa.getId(), pessoa.getNome());
        this.setUsuario(pessoa.getUsuario());
        this.setDataNascimento(pessoa.getDataNascimento());
        this.setEmail(pessoa.getEmail());
    }
}
