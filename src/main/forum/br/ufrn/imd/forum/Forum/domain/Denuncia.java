package br.ufrn.imd.forum.Forum.domain;

import br.ufrn.imd.forum.Forum.enums.TipoDenuncia;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "denuncias")
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = {"id"})
public class Denuncia {
    @Id
    @GeneratedValue
    private Long id;

    private String descricao;

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataCriado = new Date();

    @Version
    private Long versao = 0L;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_pessoa_cadastro")
    private Pessoa pessoaCadastro;

    private boolean ativo = true;

    private TipoDenuncia tipoDenuncia;

    private Long idTipo;

    @Transient
    public static final String[] ignoreProperties = {
            "id",
            "dataCriado",
            "versao",
            "pessoaCadastro",
            "ignoreProperties",
            "id_tipo",
            "tipoDenuncia"
    };
}
