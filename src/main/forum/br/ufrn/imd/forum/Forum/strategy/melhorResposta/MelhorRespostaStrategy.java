package br.ufrn.imd.forum.Forum.strategy.melhorResposta;

import br.ufrn.imd.forum.Forum.domain.Duvida;
import br.ufrn.imd.forum.Forum.domain.Pessoa;
import br.ufrn.imd.forum.Forum.domain.Resposta;
import br.ufrn.imd.forum.Forum.exception.BusinessRuleException;

public interface MelhorRespostaStrategy {
    Resposta escolherResposta(Duvida duvida, Resposta resposta, Pessoa pessoa) throws BusinessRuleException;
}
