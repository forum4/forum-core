package br.ufrn.imd.forum.Forum.domain;

import br.ufrn.imd.forum.Forum.enums.TipoAvaliacao;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "avaliacao")
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = {"id"})
public class Avaliacao {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_pessoa_cadastro")
    private Pessoa pessoaCadastro;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_resposta")
    private Resposta resposta;

    @Enumerated(EnumType.ORDINAL)
    private TipoAvaliacao tipoAvaliacao;

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataCriado = new Date();

    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataUltimaEdicao = new Date();

    @Version
    private Long versao = 0L;
}
