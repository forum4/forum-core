package br.ufrn.imd.forum.Forum.rest;

import br.ufrn.imd.forum.Forum.core.auth.users.UsuarioForum;
import br.ufrn.imd.forum.Forum.core.utils.PageResponse;
import br.ufrn.imd.forum.Forum.core.utils.QueryFilter;
import br.ufrn.imd.forum.Forum.core.utils.ValidatorUtil;
import br.ufrn.imd.forum.Forum.domain.Pessoa;
import br.ufrn.imd.forum.Forum.domain.Resposta;
import br.ufrn.imd.forum.Forum.dto.RespostaDTO;
import br.ufrn.imd.forum.Forum.exception.BusinessRuleException;
import br.ufrn.imd.forum.Forum.mapper.RespostaMapper;
import br.ufrn.imd.forum.Forum.service.NotificacaoService;
import br.ufrn.imd.forum.Forum.service.RespostaService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/resposta")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", maxAge = 3600)
public class RespostaRestController {
    private final RespostaService respostaService;
    private final NotificacaoService notificacaoService;

    @GetMapping
    public PageResponse<RespostaDTO> buscarTodos(
            @RequestParam(value = "search", required = false) String search,
            @RequestParam(value = "id_related", required = false) Long idRelacionado,
            @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @RequestParam(value = "pageSize", required = false, defaultValue = "20") int pageSize
    ) {
        QueryFilter filtro = new QueryFilter(search, idRelacionado, page, pageSize);
        PageResponse<Resposta> respostas = this.respostaService.buscarTodos(filtro);
        PageResponse<RespostaDTO> resposta = new PageResponse<>(respostas);

        resposta.setConteudo(
                respostas.getConteudo().stream().map(RespostaMapper::converter).collect(Collectors.toList())
        );

        return resposta;
    }

    @GetMapping(path = "/{id}")
    public RespostaDTO buscarUm(@PathVariable("id") Long idResposta) {
        return RespostaMapper.converter(this.respostaService.buscar(idResposta));
    }

    @PostMapping
    public Long cadastrar(UsuarioForum user, @RequestBody RespostaDTO respostaDTO) throws BusinessRuleException {
        Resposta resposta = RespostaMapper.converter(respostaDTO);

        Resposta respostaBd = this.respostaService.cadastrar(user.getId(), resposta);
        try {
            this.notificacaoService.notificar(respostaBd);
        } catch (MessagingException e) {
            // Faça nada, email não enviado não deve ser notificado para usuário que ta tentando cadastrar duvida
        }
        return respostaBd.getId();
    }

    @PutMapping
    public Long editar(UsuarioForum user, @RequestBody RespostaDTO respostaDTO) throws BusinessRuleException {
        Resposta resposta = RespostaMapper.converter(respostaDTO);

        if (ValidatorUtil.isEmpty(resposta.getPessoaCadastro())) {
            resposta.setPessoaCadastro(new Pessoa(user.getId()));
        }

        return this.respostaService.editar(user.getId(), resposta).getId();
    }

    @DeleteMapping(path = "/{id}")
    public void deletar(UsuarioForum user, @PathVariable("id") Long idResposta) throws BusinessRuleException {
        this.respostaService.deletar(user.getId(), idResposta);
    }
}
