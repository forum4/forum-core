package br.ufrn.imd.forum.Forum.factory;

import br.ufrn.imd.forum.Forum.domain.Duvida;
import br.ufrn.imd.forum.Forum.domain.Resposta;

public class FabricaNotificacaoPadrao implements FabricaNotificacao {

    @Override
    public String buildTextoNotificacao(Duvida duvida) {
        return "<h1>Nova dúvida cadastrada</h1>" +
                "<p>" + duvida.getPessoaCadastro().getNome() + " cadastrou a dúvida abaixo.</p>" +
                "<p>Dúvida: " + duvida.getTitulo() + "</p>" +
                "<p>Descrição: " + duvida.getDescricao() + "</p>";
    }

    @Override
    public String buildTextoNotificacao(Resposta resposta) {
        return "<h1>Nova resposta cadastrada</h1>" +
                "<p>" + resposta.getPessoaCadastro().getNome() + " cadastrou a resposta abaixo.</p>" +
                "<p>Resposta: " + resposta.getResposta() + "</p>";
    }
}
