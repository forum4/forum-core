package br.ufrn.imd.forum.Forum.mapper;

import br.ufrn.imd.forum.Forum.domain.Duvida;
import br.ufrn.imd.forum.Forum.domain.Resposta;
import br.ufrn.imd.forum.Forum.dto.RespostaDTO;
import br.ufrn.imd.forum.Forum.dto.TipoDTO;
import org.springframework.beans.BeanUtils;

public class RespostaMapper {
    public static Resposta converter(RespostaDTO respostaDTO) {
        Resposta resposta = new Resposta();
        BeanUtils.copyProperties(respostaDTO, resposta);

        resposta.setId(respostaDTO.getId());

        resposta.setDuvida(new Duvida());
        resposta.getDuvida().setId(respostaDTO.getDuvida().getId());

        return resposta;
    }

    public static RespostaDTO converter(Resposta resposta) {
        RespostaDTO respostaDTO = new RespostaDTO();
        BeanUtils.copyProperties(resposta, respostaDTO);

        respostaDTO.setId(resposta.getId());

        respostaDTO.setDuvida(
                new TipoDTO<>(resposta.getDuvida().getId(), resposta.getDuvida().getTitulo())
        );

        respostaDTO.setPessoaCadastro(
                new TipoDTO<>(resposta.getPessoaCadastro().getId(), resposta.getPessoaCadastro().getNome())
        );

        respostaDTO.setPessoaUltimaEdicao(
                new TipoDTO<>(resposta.getPessoaUltimaEdicao().getId(), resposta.getPessoaUltimaEdicao().getNome())
        );

        return respostaDTO;
    }
}
