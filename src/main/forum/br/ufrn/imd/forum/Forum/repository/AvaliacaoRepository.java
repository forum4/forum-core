package br.ufrn.imd.forum.Forum.repository;

import br.ufrn.imd.forum.Forum.domain.Avaliacao;
import br.ufrn.imd.forum.Forum.domain.Pessoa;
import br.ufrn.imd.forum.Forum.domain.Resposta;
import br.ufrn.imd.forum.Forum.enums.TipoAvaliacao;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AvaliacaoRepository extends JpaRepository<Avaliacao, Long> {
    Optional<Avaliacao> findByPessoaCadastroAndResposta(Pessoa pessoaCadastro, Resposta resposta);

    int countDistinctByRespostaAndTipoAvaliacao(Resposta resposta, TipoAvaliacao tipoAvaliacao);
}
