package br.ufrn.imd.forum.Forum.rest;

import br.ufrn.imd.forum.Forum.core.auth.users.UsuarioForum;
import br.ufrn.imd.forum.Forum.core.utils.ValidatorUtil;
import br.ufrn.imd.forum.Forum.domain.Duvida;
import br.ufrn.imd.forum.Forum.domain.Notificacao;
import br.ufrn.imd.forum.Forum.domain.Pessoa;
import br.ufrn.imd.forum.Forum.domain.TopicoForum;
import br.ufrn.imd.forum.Forum.exception.BusinessRuleException;
import br.ufrn.imd.forum.Forum.service.DuvidaService;
import br.ufrn.imd.forum.Forum.service.NotificacaoService;
import br.ufrn.imd.forum.Forum.service.PessoaService;
import br.ufrn.imd.forum.Forum.service.TopicoForumService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/notificacao")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", maxAge = 3600)
public class NotificacaoRestController {
    private final NotificacaoService notificacaoService;
    private final PessoaService pessoaService;
    private final TopicoForumService topicoForumService;
    private final DuvidaService duvidaService;

    @PostMapping
    public Long cadastrar(
            UsuarioForum user,
            @RequestParam(value = "id_topico_forum", required = false) Long idTopicoForum,
            @RequestParam(value = "id_duvida", required = false) Long idDuvida
    ) throws BusinessRuleException {
        this.validarParametros(idTopicoForum, idDuvida);

        Pessoa pessoa = this.pessoaService.buscar(user.getId());

        Notificacao notificacao;
        if (ValidatorUtil.isNotEmpty(idTopicoForum)) {
            TopicoForum topicoForum = this.topicoForumService.buscar(idTopicoForum);
            notificacao = this.notificacaoService.cadastrar(pessoa, topicoForum);
        } else {
            Duvida duvida = this.duvidaService.buscar(idDuvida);
            notificacao = this.notificacaoService.cadastrar(pessoa, duvida);
        }

        return notificacao.getId();
    }

    private void validarParametros(Long idTopicoForum, Long idDuvida) throws BusinessRuleException {
        if (ValidatorUtil.isEmpty(idTopicoForum) && ValidatorUtil.isEmpty(idDuvida)) {
            throw new BusinessRuleException("Tópico de Fórum e Dúvida faltando");
        }

        if (ValidatorUtil.isAllNotEmpty(idTopicoForum, idDuvida)) {
            throw new BusinessRuleException("Escolha apenas ou um Tópico de Fórum ou uma Dúvida");
        }
    }
}
