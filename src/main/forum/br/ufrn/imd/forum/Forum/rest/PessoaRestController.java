package br.ufrn.imd.forum.Forum.rest;

import br.ufrn.imd.forum.Forum.core.auth.users.UsuarioForum;
import br.ufrn.imd.forum.Forum.core.utils.PageResponse;
import br.ufrn.imd.forum.Forum.core.utils.QueryFilter;
import br.ufrn.imd.forum.Forum.domain.Pessoa;
import br.ufrn.imd.forum.Forum.dto.PessoaDTO;
import br.ufrn.imd.forum.Forum.exception.BusinessRuleException;
import br.ufrn.imd.forum.Forum.service.PessoaService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/pessoa")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", maxAge = 3600)
public class PessoaRestController {
    private final PessoaService pessoaService;

    @GetMapping
    public PageResponse<PessoaDTO> buscarTodos(
            @RequestParam(value = "search", required = false) String search,
            @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @RequestParam(value = "pageSize", required = false, defaultValue = "20") int pageSize
    ) {
        QueryFilter filtro = new QueryFilter(search, null, page, pageSize);
        PageResponse<Pessoa> pessoas = this.pessoaService.buscarTodos(filtro);
        PageResponse<PessoaDTO> resposta = new PageResponse<>(pessoas);
        resposta.setConteudo(
                pessoas.getConteudo().stream().map(PessoaDTO::new).collect(Collectors.toList())
        );
        return resposta;
    }

    @GetMapping(path = "/{id}")
    public PessoaDTO buscarUm(@PathVariable("id") Pessoa pessoa) {
        return new PessoaDTO(this.pessoaService.buscar(pessoa.getId()));
    }

    @GetMapping(path = "/existe-usuario")
    public boolean existsPessoaComNomeUsuarioEmail(
            @RequestParam(value = "pessoa_id", required = false) Long id,
            @RequestParam("username") String username
    ) {
        return this.pessoaService.existsPessoaPorUsername(id, username);
    }

    @GetMapping(path = "/existe-email")
    public boolean existsPessoaComEmail(
            @RequestParam(value = "pessoa_id", required = false) Long id,
            @RequestParam("email") String email
    ) {
        return this.pessoaService.existsPessoaPorEmail(id, email);
    }

    @PostMapping
    public Long salvar(@RequestBody Pessoa pessoa) throws BusinessRuleException {
        return this.pessoaService.salvar(pessoa).getId();
    }

    @DeleteMapping
    public void deletar(UsuarioForum user) {
        this.pessoaService.deletar(user.getId());
    }
}

