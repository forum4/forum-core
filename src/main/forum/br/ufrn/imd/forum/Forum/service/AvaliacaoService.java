package br.ufrn.imd.forum.Forum.service;

import br.ufrn.imd.forum.Forum.core.utils.ValidatorUtil;
import br.ufrn.imd.forum.Forum.domain.Avaliacao;
import br.ufrn.imd.forum.Forum.domain.Pessoa;
import br.ufrn.imd.forum.Forum.domain.Resposta;
import br.ufrn.imd.forum.Forum.enums.TipoAvaliacao;
import br.ufrn.imd.forum.Forum.exception.BusinessRuleException;
import br.ufrn.imd.forum.Forum.repository.AvaliacaoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AvaliacaoService {
    private final AvaliacaoRepository avaliacaoRepository;
    private final PessoaService pessoaService;
    private final RespostaService respostaService;

    private int countAvaliacoesPorResposta(Long idResposta, TipoAvaliacao tipoAvaliacao) {
        Resposta resposta = this.respostaService.buscar(idResposta);
        return this.avaliacaoRepository.countDistinctByRespostaAndTipoAvaliacao(resposta, tipoAvaliacao);
    }

    public double mediaAvaliacoesPorResposta(Long idResposta) {
        int likes = this.countLikesPorResposta(idResposta);
        int dislikes = this.countDislikesPorResposta(idResposta);

        return ((double) likes) / (likes + dislikes);
    }

    public int countLikesPorResposta(Long idResposta) {
        return this.countAvaliacoesPorResposta(idResposta, TipoAvaliacao.LIKE);
    }

    public int countDislikesPorResposta(Long idResposta) {
        return this.countAvaliacoesPorResposta(idResposta, TipoAvaliacao.DISLIKE);
    }

    public Avaliacao buscar(Long idUsuario, Long idResposta) {
        Pessoa pessoa = this.pessoaService.buscar(idUsuario);
        Resposta resposta = this.respostaService.buscar(idResposta);
        return this.avaliacaoRepository.findByPessoaCadastroAndResposta(pessoa, resposta).orElseThrow(() -> new ResourceNotFoundException("Avaliação não encontrada"));
    }

    private Avaliacao criarAvaliacao(Long idUsuario, Long idResposta, TipoAvaliacao tipoAvaliacao) {
        Avaliacao avaliacao = new Avaliacao();

        try {
            avaliacao = this.buscar(idUsuario, idResposta);
        } catch (ResourceNotFoundException ignored) {
            // Não há avaliação desse usuário para essa resposta
            Pessoa pessoa = this.pessoaService.buscar(idUsuario);
            Resposta resposta = this.respostaService.buscar(idResposta);

            avaliacao.setPessoaCadastro(pessoa);
            avaliacao.setResposta(resposta);
        } finally {
            avaliacao.setTipoAvaliacao(tipoAvaliacao);
        }

        return avaliacao;
    }

    @Transactional
    public Avaliacao salvar(Long idUsuario, Long idResposta, TipoAvaliacao tipoAvaliacao) throws BusinessRuleException {
        Avaliacao avaliacao = this.criarAvaliacao(idUsuario, idResposta, tipoAvaliacao);

        this.validar(avaliacao);

        return this.avaliacaoRepository.save(avaliacao);
    }

    private void validar(Avaliacao avaliacao) throws BusinessRuleException {
        List<String> messages = new ArrayList<>();
        if (ValidatorUtil.isEmpty(avaliacao)) {
            messages.add("Avaliação não pode ser vazia");
            // Entidade nula, então lança logo exception
            throw new BusinessRuleException(messages);
        }

        if (ValidatorUtil.isEmpty(avaliacao.getPessoaCadastro())) {
            messages.add("Avaliação não vinculada a uma pessoa");
        }

        if (ValidatorUtil.isEmpty(avaliacao.getResposta())) {
            messages.add("Avaliação não vinculada a uma resposta");
        }

        if (ValidatorUtil.isEmpty(avaliacao.getTipoAvaliacao())) {
            messages.add("Tipo de Avaliação não pode ser vazio");
        }

        if (ValidatorUtil.isNotEmpty(messages)) {
            throw new BusinessRuleException(messages);
        }
    }

    @Transactional
    public void deletar(Long idUsuario, Long idResposta) throws BusinessRuleException {
        Avaliacao avaliacao = this.buscar(idUsuario, idResposta);
        this.avaliacaoRepository.delete(avaliacao);
    }
}
