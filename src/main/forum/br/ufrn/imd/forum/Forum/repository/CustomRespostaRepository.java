package br.ufrn.imd.forum.Forum.repository;

import br.ufrn.imd.forum.Forum.domain.Resposta;

import java.util.List;

public interface CustomRespostaRepository {
    List<Resposta> findAllByDuvidaPageable(Long idDuvida, int pageSize, int skipRows);
}
