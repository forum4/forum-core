package br.ufrn.imd.forum.Forum.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "tags")
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = {"id"})
public class Tag {
    @Id
    @GeneratedValue
    private Long id;

    @Version
    private Long versao = 0L;

    private String descricao;

    private boolean ativo = true;


    @Transient
    public static final String[] ignoreProperties = {
            "id",
            "versao",
            "ignoreProperties",
    };
}
