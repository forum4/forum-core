package br.ufrn.imd.forum.Forum.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class DuvidaDTO extends TipoDTO<Long>{
    private String titulo;
    private String descricao;
    private Date dataCriado;
    private Date dataUltimaEdicao;
    private TipoDTO<Long> respostaCorreta;
    private TipoDTO<Long> pessoaCadastro;
    private TipoDTO<Long> pessoaUltimaEdicao;
    private TipoDTO<Long> topicoForum;
    private List<TipoDTO<Long>> tags;
}
