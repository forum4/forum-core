package br.ufrn.imd.forum.Forum.repository.impl;

import br.ufrn.imd.forum.Forum.domain.Resposta;
import br.ufrn.imd.forum.Forum.mapper.row.RespostaRowMapper;
import br.ufrn.imd.forum.Forum.repository.CustomRespostaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.List;

@RequiredArgsConstructor
public class CustomRespostaRepositoryImpl implements CustomRespostaRepository {
    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public List<Resposta> findAllByDuvidaPageable(Long idDuvida, int pageSize, int skipRows) {
        String query = "SELECT DISTINCT res.id, res.resposta, res.ativo, res.data_criado, res.data_ultima_edicao, res.versao, " +
                "duv.id as id_duvida, duv.titulo as titulo_duvida, pes.id as id_pessoa_cadastro, pes.nome as nome_pessoa_cadastro, " +
                "pesU.id as id_pessoa_update, pesU.nome as nome_pessoa_update, " +
                "(select count(alike.id) from avaliacao alike where alike.id_resposta = res.id and alike.tipo_avaliacao = 0) as likes, " +
                "(select count(adislike.id) from avaliacao adislike where adislike.id_resposta = res.id and adislike.tipo_avaliacao = 1) as dislikes " +
                "FROM resposta res " +
                "JOIN duvida duv ON duv.id = res.id_duvida " +
                "JOIN pessoa pes ON pes.id = res.id_pessoa_cadastro " +
                "JOIN pessoa pesU ON pesU.id = res.id_pessoa_ultima_edicao " +
                "WHERE res.id_duvida = :id_duvida AND res.ativo = TRUE " +
                "ORDER BY res.data_criado ASC " +
                "LIMIT :pageSize OFFSET :skipRows";

        MapSqlParameterSource namedParameter = new MapSqlParameterSource();
        namedParameter.addValue("id_duvida", idDuvida);
        namedParameter.addValue("pageSize", pageSize);
        namedParameter.addValue("skipRows", skipRows);

        return this.jdbcTemplate.query(query, namedParameter, new RespostaRowMapper());
    }
}
