package br.ufrn.imd.forum.Forum.service;

import br.ufrn.imd.forum.Forum.core.utils.PageResponse;
import br.ufrn.imd.forum.Forum.core.utils.QueryFilter;
import br.ufrn.imd.forum.Forum.core.utils.ValidatorUtil;
import br.ufrn.imd.forum.Forum.domain.*;
import br.ufrn.imd.forum.Forum.exception.BusinessRuleException;
import br.ufrn.imd.forum.Forum.repository.DuvidaRepository;
import br.ufrn.imd.forum.Forum.service.interfaces.ServiceInterfaceProtected;
import br.ufrn.imd.forum.Forum.strategy.melhorResposta.MelhorRespostaStrategy;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DuvidaService implements ServiceInterfaceProtected<Duvida, Long> {
    private final DuvidaRepository duvidaRepository;
    private final PessoaService pessoaService;
    private final TopicoForumService topicoForumService;
    private final RespostaService respostaService;
    private final TagService tagService;
    private final MelhorRespostaStrategy melhorResposta;

    @Override
    public PageResponse<Duvida> buscarTodos(QueryFilter filtro) {
        PageResponse<Duvida> response = new PageResponse<>();
        int skipRows = (filtro.getPage() - 1) * filtro.getPageSize();

        Long idTopicoForum = filtro.getIdRelacionado();
        List<Duvida> duvidas = this.duvidaRepository.findAllByTopicoForumPageable(idTopicoForum, filtro.getPageSize(), skipRows);
        Integer total = this.duvidaRepository.countDistinctByTopicoForumPageable(idTopicoForum);

        if (ValidatorUtil.isEmpty(total)) {
            total = 0;
        }

        response.setConteudo(duvidas);
        response.setPaginaAtual(filtro.getPage());
        response.setTamanhoPagina(filtro.getPageSize());
        response.setTotalElementos(total.longValue());
        response.setTotalPaginas((long) (total / filtro.getPageSize()));

        return response;
    }

    @Override
    public Duvida buscar(Long id) {
        return this.duvidaRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Dúvida não encontrada!"));
    }

    public List<Duvida> buscarPorTags(List<Long> idsTags) {
        List<Tag> tags = idsTags.stream().map(tagService::buscar).collect(Collectors.toList());
        return this.duvidaRepository.findByTagsInAndAtivoTrueOrderByDataCriado(tags);
    }


    @Override
    public Duvida salvar(Long idPessoa, Duvida duvida) throws BusinessRuleException {
        Pessoa pessoa = this.pessoaService.buscar(idPessoa);
        duvida.setPessoaUltimaEdicao(pessoa);
        duvida.setDataUltimaEdicao(new Date());

        this.validarDuvida(duvida);

        return this.duvidaRepository.save(duvida);
    }

    private void validarDuvida(Duvida duvida) throws BusinessRuleException {
        if (ValidatorUtil.isEmpty(duvida.getTitulo())) {
            throw new BusinessRuleException("Título não pode ser vazio");
        }

        if (ValidatorUtil.isEmpty(duvida.getPessoaCadastro()) || ValidatorUtil.isEmpty(duvida.getPessoaUltimaEdicao())) {
            throw new BusinessRuleException("Dúvida não associada a um usuário");
        }
    }

    @Transactional
    public Duvida cadastrar(Long idPessoa, Duvida duvida) throws BusinessRuleException {
        Pessoa pessoa = this.pessoaService.buscar(idPessoa);
        TopicoForum topicoForum = this.topicoForumService.buscar(duvida.getTopicoForum().getId());
        duvida.setPessoaCadastro(pessoa);
        duvida.setDataCriado(new Date());
        duvida.setTopicoForum(topicoForum);

        return this.salvar(idPessoa, duvida);
    }

    @Transactional
    public Duvida editar(Long idPessoa, Duvida duvida) throws BusinessRuleException {
        Duvida duvidaBd = this.buscar(duvida.getId());
        BeanUtils.copyProperties(duvida, duvidaBd, Duvida.ignoreProperties);

        return this.salvar(idPessoa, duvidaBd);
    }

    public void deletar(Long idPessoa, Long idDuvida) {
        Pessoa pessoa = this.pessoaService.buscar(idPessoa);
        Duvida duvida = this.buscar(idDuvida);
        duvida.setPessoaUltimaEdicao(pessoa);
        duvida.setDataUltimaEdicao(new Date());

        this.desativarDuvida(duvida);

    }

    @Transactional
    public void desativarDuvida(Duvida duvida) {
        duvida.setAtivo(false);
        this.duvidaRepository.save(duvida);
    }

    @Transactional
    public void marcarComoRespondida(Long idPessoa, Long idDuvida, Long idResposta) throws BusinessRuleException {
        Pessoa pessoa = this.pessoaService.buscar(idPessoa);
        Duvida duvida = this.buscar(idDuvida);
        Resposta resposta = this.respostaService.buscar(idResposta);

        duvida.setRespostaCorreta(melhorResposta.escolherResposta(duvida, resposta, pessoa));
        this.salvar(idPessoa, duvida);
    }
}
