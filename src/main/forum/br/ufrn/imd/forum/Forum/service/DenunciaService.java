package br.ufrn.imd.forum.Forum.service;

import br.ufrn.imd.forum.Forum.core.utils.PageResponse;
import br.ufrn.imd.forum.Forum.core.utils.QueryFilter;
import br.ufrn.imd.forum.Forum.core.utils.ValidatorUtil;
import br.ufrn.imd.forum.Forum.domain.*;
import br.ufrn.imd.forum.Forum.exception.BusinessRuleException;
import br.ufrn.imd.forum.Forum.repository.DenunciaRepository;
import br.ufrn.imd.forum.Forum.service.interfaces.ServiceInterfaceProtected;
import br.ufrn.imd.forum.Forum.strategy.punicoes.PunicaoStrategy;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DenunciaService implements ServiceInterfaceProtected<Denuncia, Long> {
    private final DenunciaRepository denunciaRepository;
    private final PessoaService pessoaService;
    private final TopicoForumService topicoForumService;
    private final DuvidaService duvidaService;
    private final RespostaService respostaService;
    private final PunicaoStrategy punicao;


    @Override
    public PageResponse<Denuncia> buscarTodos(QueryFilter filtro) {
        PageResponse<Denuncia> response = new PageResponse<>();
        int skipRows = (filtro.getPage() - 1) * filtro.getPageSize();

        Long idPessoa = filtro.getIdRelacionado();
        List<Denuncia> denuncias = this.denunciaRepository.findAllByUsuarioPageable(idPessoa, filtro.getPageSize(), skipRows);
        Integer total = this.denunciaRepository.countDistinctByUsuarioPageable(idPessoa);

        if (ValidatorUtil.isEmpty(total)) {
            total = 0;
        }

        response.setConteudo(denuncias);
        response.setPaginaAtual(filtro.getPage());
        response.setTamanhoPagina(filtro.getPageSize());
        response.setTotalElementos(total.longValue());
        response.setTotalPaginas((long) (total / filtro.getPageSize()));

        return response;
    }

    @Override
    public Denuncia buscar(Long id) {
        return this.denunciaRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Denúncia não encontrada!")
        );
    }

    public Denuncia cadastrar(Long idUsuario, Denuncia entidade) throws BusinessRuleException {
        Pessoa pessoa = this.pessoaService.buscar(idUsuario);
        entidade.setPessoaCadastro(pessoa);
        entidade.setDataCriado(new Date());

        if (this.denunciaRepository.countDistinctDenunciaByTipoAndPessoaCadastro(entidade.getTipoDenuncia(),
                entidade.getIdTipo(), pessoa) > 0) {
            throw new BusinessRuleException("Você ja realizou uma denuncia");
        }
        Denuncia denunciaSalva =  this.salvar(idUsuario, entidade);

        switch (entidade.getTipoDenuncia()) {
            case DUVIDA:
                Duvida duvidaDenunciada = this.duvidaService.buscar(entidade.getIdTipo());
                punicao.punir(duvidaDenunciada);
                break;
            case RESPOSTA:
                Resposta respostaDenunciada = this.respostaService.buscar(entidade.getIdTipo());
                punicao.punir(respostaDenunciada);
                break;
            case TOPICO_FORUM:
                TopicoForum topicoDenunciado = this.topicoForumService.buscar(entidade.getIdTipo());
                punicao.punir(topicoDenunciado);
                break;
            default:
                break;
        }
        return denunciaSalva;
    }

    public Denuncia editar(Long idUsuario, Denuncia entidade) throws BusinessRuleException {
        Denuncia denunciaBD = this.buscar(entidade.getId());
        BeanUtils.copyProperties(entidade, denunciaBD, Denuncia.ignoreProperties);
        denunciaBD.setDataCriado(denunciaBD.getDataCriado());
        return this.salvar(idUsuario, entidade);
    }

    @Override
    public Denuncia salvar(Long idUsuario, Denuncia entidade) throws BusinessRuleException {
        this.validarDenuncia(entidade);

        return this.denunciaRepository.save(entidade);

    }

    @Override
    public void deletar(Long idPessoa, Long idDenuncia) throws BusinessRuleException {
        Denuncia denuncia = this.buscar(idDenuncia);
        denuncia.setAtivo(false);

        this.denunciaRepository.save(denuncia);
    }

    private void validarDenuncia(Denuncia denuncia) throws BusinessRuleException {
        if (ValidatorUtil.isEmpty(denuncia.getDescricao())) {
            throw new BusinessRuleException("O comentário da denuncia não pode ser vazio");
        }

        if (ValidatorUtil.isNotEmpty(denuncia.getId()) && ValidatorUtil.isEmpty(denuncia.getPessoaCadastro())) {
            throw new BusinessRuleException("Denúncia não associada a um usuário");
        }
    }
}
