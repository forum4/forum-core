package br.ufrn.imd.forum.Forum.mapper;

import br.ufrn.imd.forum.Forum.domain.Denuncia;
import br.ufrn.imd.forum.Forum.domain.Tag;
import br.ufrn.imd.forum.Forum.dto.DenunciaDTO;
import br.ufrn.imd.forum.Forum.dto.TagDTO;
import br.ufrn.imd.forum.Forum.dto.TipoDTO;
import org.springframework.beans.BeanUtils;

public class TagMapper {
    public static Tag converter(TagDTO tagDTO) {
        Tag tag = new Tag();

        BeanUtils.copyProperties(tagDTO, tag);
        tag.setId(tagDTO.getId());

        return tag;
    }

    public static TagDTO converter(Tag tag) {
        TagDTO tagDTO = new TagDTO();

        BeanUtils.copyProperties(tag, tagDTO);
        tagDTO.setId(tag.getId());

        return tagDTO;
    }
}
