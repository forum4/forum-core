package br.ufrn.imd.forum.Forum.domain;

import br.ufrn.imd.forum.Forum.enums.TipoPapel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "pessoa")
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = {"id"})
public class Pessoa {
    @Id
    @GeneratedValue
    private Long id;

    private String nome;

    private String usuario;

    private String senha;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dataNascimento;

    private String email;

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataCriado = new Date();

    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataUltimaEdicao = new Date();

    @Version
    private Long versao = 0L;

    private boolean ativo = true;

    @Enumerated(EnumType.STRING)
    private TipoPapel papel = TipoPapel.COMUM;

    @Transient
    public static final String[] ignoreProperties = {"senha", "dataCriado", "dataUltimaEdicao", "versao", "ativo", "ignoreProperties"};

    public Pessoa(Long id) {
        this.setId(id);
    }

    public Pessoa(Long id, String nome) {
        this(id);
        this.setNome(nome);
    }
}
