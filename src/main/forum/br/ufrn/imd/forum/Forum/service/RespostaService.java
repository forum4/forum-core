package br.ufrn.imd.forum.Forum.service;

import br.ufrn.imd.forum.Forum.core.utils.PageResponse;
import br.ufrn.imd.forum.Forum.core.utils.QueryFilter;
import br.ufrn.imd.forum.Forum.core.utils.ValidatorUtil;
import br.ufrn.imd.forum.Forum.domain.Duvida;
import br.ufrn.imd.forum.Forum.domain.Pessoa;
import br.ufrn.imd.forum.Forum.domain.Resposta;
import br.ufrn.imd.forum.Forum.exception.BusinessRuleException;
import br.ufrn.imd.forum.Forum.repository.DuvidaRepository;
import br.ufrn.imd.forum.Forum.repository.RespostaRepository;
import br.ufrn.imd.forum.Forum.service.interfaces.ServiceInterfaceProtected;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RespostaService implements ServiceInterfaceProtected<Resposta, Long> {
    private final RespostaRepository respostaRepository;
    private final PessoaService pessoaService;
    private final DuvidaRepository duvidaRepository;

    @Override
    public Resposta buscar (Long id){
        return this.respostaRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Resposta não encontrada!")
        );
    }

    @Override
    public PageResponse<Resposta> buscarTodos(QueryFilter filtro) {
        PageResponse<Resposta> response = new PageResponse<>();
        int skipRows = (filtro.getPage() - 1) * filtro.getPageSize();

        Long idDuvida = filtro.getIdRelacionado();
        List<Resposta> respostas = this.respostaRepository.findAllByDuvidaPageable(idDuvida, filtro.getPageSize(), skipRows);
        Integer total = this.respostaRepository.countDistinctByDuvidaPageable(idDuvida);

        if (ValidatorUtil.isEmpty(total)) {
            total = 0;
        }

        response.setConteudo(respostas);
        response.setPaginaAtual(filtro.getPage());
        response.setTamanhoPagina(filtro.getPageSize());
        response.setTotalElementos(total.longValue());
        response.setTotalPaginas((long) (total / filtro.getPageSize()));

        return response;
    }

    public Resposta cadastrar (Long idPessoa, Resposta resposta) throws BusinessRuleException {

        Pessoa pessoa = this.pessoaService.buscar(idPessoa);
        Duvida duvida = this.duvidaRepository.findById(resposta.getDuvida().getId()).orElseThrow(() -> new ResourceNotFoundException("Dúvida não encontrada"));

        if (ValidatorUtil.isEmpty(resposta.getPessoaCadastro())) {
            resposta.setPessoaCadastro(pessoa);
        }

        if (ValidatorUtil.isEmpty(resposta.getPessoaUltimaEdicao())) {
            resposta.setPessoaUltimaEdicao(pessoa);
        }

        resposta.setDataCriado(new Date());
        resposta.setDuvida(duvida);

        return this.salvar(idPessoa, resposta);

    }

    public Resposta editar (Long idPessoa, Resposta resposta) throws BusinessRuleException {
        Resposta respostaBd = this.buscar(resposta.getId());
        BeanUtils.copyProperties(resposta, respostaBd, Resposta.ignoreProperties);
        resposta.setDataCriado(respostaBd.getDataCriado());
        return this.salvar(idPessoa, resposta);
    }

    @Override
    @Transactional
    public Resposta salvar(Long idUsuario, Resposta resposta) throws BusinessRuleException {
        Pessoa pessoa = this.pessoaService.buscar(idUsuario);
        resposta.setPessoaUltimaEdicao(pessoa);
        resposta.setDataUltimaEdicao(new Date());

        this.validarResposta(resposta);

        return this.respostaRepository.save(resposta);
    }

    private void validarResposta(Resposta resposta) throws BusinessRuleException {
        if (ValidatorUtil.isEmpty(resposta.getResposta())) {
            throw new BusinessRuleException("A resposta não pode ser vazio");
        }

        if (ValidatorUtil.isNotEmpty(resposta.getId()) && ValidatorUtil.isEmpty(resposta.getPessoaCadastro())) {
            throw new BusinessRuleException("Resposta não associada a um usuário");
        }
    }

    public void deletar (Long idPessoa, Long idResposta) {
        Pessoa pessoa = this.pessoaService.buscar(idPessoa);
        Resposta resposta = this.buscar(idResposta);
        resposta.setPessoaUltimaEdicao(pessoa);
        resposta.setDataUltimaEdicao(new Date());
        this.desativarResposta(resposta);

    }

    @Transactional
    public void desativarResposta(Resposta resposta) {
        resposta.setAtivo(false);
        this.respostaRepository.save(resposta);
    }
}
