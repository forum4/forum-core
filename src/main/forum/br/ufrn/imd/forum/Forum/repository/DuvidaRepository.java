package br.ufrn.imd.forum.Forum.repository;

import br.ufrn.imd.forum.Forum.domain.Duvida;
import br.ufrn.imd.forum.Forum.domain.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DuvidaRepository extends JpaRepository<Duvida, Long> {
    @Query(nativeQuery = true, value = "SELECT DISTINCT d.* FROM duvida d WHERE d.id_topico_forum = :id_topico AND d.ativo = TRUE ORDER BY d.data_criado DESC LIMIT :pageSize OFFSET :skipRows")
    List<Duvida> findAllByTopicoForumPageable(@Param("id_topico") Long idTopico, @Param("pageSize") int pageSize, @Param("skipRows") int skipRows);

    @Query(nativeQuery = true, value = "SELECT COUNT(DISTINCT d.id) FROM duvida d WHERE d.id_topico_forum = :id_topico AND d.ativo = TRUE")
    Integer countDistinctByTopicoForumPageable(@Param("id_topico") Long idTopico);

    List<Duvida> findByTagsInAndAtivoTrueOrderByDataCriado(@Param("tags")List<Tag> tagList);
}
