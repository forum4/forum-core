package br.ufrn.imd.forum.Forum.factory;

import br.ufrn.imd.forum.Forum.domain.Duvida;
import br.ufrn.imd.forum.Forum.domain.Resposta;

public interface FabricaNotificacao {
    String buildTextoNotificacao(Duvida duvida);

    String buildTextoNotificacao(Resposta resposta);
}
