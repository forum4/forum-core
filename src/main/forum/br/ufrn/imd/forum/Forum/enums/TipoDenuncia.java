package br.ufrn.imd.forum.Forum.enums;

public enum TipoDenuncia {
    TOPICO_FORUM, DUVIDA, RESPOSTA
}
