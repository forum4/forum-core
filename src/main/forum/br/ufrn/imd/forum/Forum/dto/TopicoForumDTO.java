package br.ufrn.imd.forum.Forum.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TopicoForumDTO extends TipoDTO<Long> {
    private String descricao;
    private TipoDTO<Long> pessoaCadastro;
    private Date dataCriado;
    private List<TipoDTO<Long>> tags;
}
