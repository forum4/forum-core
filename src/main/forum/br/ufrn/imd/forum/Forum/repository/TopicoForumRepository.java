package br.ufrn.imd.forum.Forum.repository;

import br.ufrn.imd.forum.Forum.domain.Duvida;
import br.ufrn.imd.forum.Forum.domain.Tag;
import br.ufrn.imd.forum.Forum.domain.TopicoForum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TopicoForumRepository extends JpaRepository<TopicoForum, Long> {

    @Query(value = "SELECT DISTINCT tf.* FROM topico_forum tf WHERE (LOWER(tf.titulo) LIKE :search OR LOWER(tf.descricao) LIKE :search) AND tf.ativo = TRUE ORDER BY tf.data_criado DESC LIMIT :pageSize OFFSET :skipRows", nativeQuery = true)
    List<TopicoForum> findAllByTituloOrDescricaoPageable(@Param("search") String search, @Param("pageSize") int pageSize, @Param("skipRows") int skipRows);

    @Query(value = "SELECT COUNT(DISTINCT tf.id) FROM TopicoForum tf WHERE (LOWER(tf.titulo) LIKE :search OR LOWER(tf.descricao) LIKE :search) AND tf.ativo = TRUE")
    Integer countDistinctByTituloOrDescricaoPageable(@Param("search") String search);

    List<TopicoForum> findDistinctByTagsInAndAtivoTrueOrderByDataCriado(@Param("tags")List<Tag> tagList);
}
