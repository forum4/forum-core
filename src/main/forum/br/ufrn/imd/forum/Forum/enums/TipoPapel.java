package br.ufrn.imd.forum.Forum.enums;

import lombok.Getter;

@Getter
public enum TipoPapel {
    ADMINISTRADOR("Administrador"),
    COMUM("Comum");

    private String descricao;

    TipoPapel(String descricao) {
        this.descricao = descricao;
    }
}
