package br.ufrn.imd.forum.Forum.strategy.melhorResposta;

import br.ufrn.imd.forum.Forum.domain.Duvida;
import br.ufrn.imd.forum.Forum.domain.Pessoa;
import br.ufrn.imd.forum.Forum.domain.Resposta;
import br.ufrn.imd.forum.Forum.exception.BusinessRuleException;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class MelhorRespostaUsuario implements MelhorRespostaStrategy {

    @Override
    public Resposta escolherResposta(Duvida duvida, Resposta resposta, Pessoa pessoa) throws BusinessRuleException {

        if (!duvida.getPessoaCadastro().equals(pessoa)) {
            throw new BusinessRuleException("Este usuário não tem permissão para alterar essa dúvida");
        }

        return resposta;
    }
}
