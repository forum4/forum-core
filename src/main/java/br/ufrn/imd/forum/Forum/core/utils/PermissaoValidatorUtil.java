package br.ufrn.imd.forum.Forum.core.utils;

import br.ufrn.imd.forum.Forum.domain.Pessoa;
import br.ufrn.imd.forum.Forum.enums.TipoPapel;
import br.ufrn.imd.forum.Forum.exception.BusinessRuleException;

public class PermissaoValidatorUtil {
    /**
     * Verificar se a pessoa tem permissão de exclusão
     *
     * @param usuario          - Usuário logado
     * @param idPessoaCadastro - Id da pessoa que cadastrou a entidade
     * @throws BusinessRuleException - Se a pessoa não tem permissão de exclusão daquela entidade
     */
    public static void permiteExcluir(Pessoa usuario, Long idPessoaCadastro) throws BusinessRuleException {
        if (!usuario.getPapel().equals(TipoPapel.ADMINISTRADOR) && !usuario.getId().equals(idPessoaCadastro)) {
            throw new BusinessRuleException("Você não tem permissão para excluir este objeto");
        }
    }
}
