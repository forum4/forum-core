package br.ufrn.imd.forum.Forum.core.config;

import br.ufrn.imd.forum.Forum.strategy.melhorResposta.MelhorRespostaStrategy;
import br.ufrn.imd.forum.Forum.strategy.melhorResposta.MelhorRespostaUsuario;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class MelhorRespostaConfiguration {

    @Bean
    public MelhorRespostaStrategy melhorResposta() {
        return new MelhorRespostaUsuario();
    }
}
