package br.ufrn.imd.forum.Forum.core.utils;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class PageResponse<T> {
    private List<T> conteudo = new ArrayList<>();
    private int paginaAtual = 1;
    private int tamanhoPagina = 20;
    private Long totalElementos = 0L;
    private Long totalPaginas = 0L;

    public PageResponse(PageResponse<?> pageResponse) {
        this.setPaginaAtual(pageResponse.getPaginaAtual());
        this.setTamanhoPagina(pageResponse.getTamanhoPagina());
        this.setTotalElementos(pageResponse.getTotalElementos());
        this.setTotalPaginas(pageResponse.getTotalPaginas());
    }
}
