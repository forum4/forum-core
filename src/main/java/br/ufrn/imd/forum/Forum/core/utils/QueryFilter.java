package br.ufrn.imd.forum.Forum.core.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class QueryFilter implements Serializable {
    private String search;
    private Long idRelacionado;
    private int page = 1;
    private int pageSize = 20;
}
