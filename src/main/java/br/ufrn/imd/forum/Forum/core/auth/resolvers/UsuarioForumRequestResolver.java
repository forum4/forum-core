package br.ufrn.imd.forum.Forum.core.auth.resolvers;

import br.ufrn.imd.forum.Forum.core.auth.exception.AuthorizationException;
import br.ufrn.imd.forum.Forum.core.auth.users.UsuarioForum;
import br.ufrn.imd.forum.Forum.core.auth.validators.JwtValidatorUtils;
import br.ufrn.imd.forum.Forum.core.utils.ValidatorUtil;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class UsuarioForumRequestResolver {
    private String getJwtToken(HttpServletRequest request) throws AuthorizationException {
        String authHeader = request.getHeader("Authorization");
        if (ValidatorUtil.isEmpty(authHeader)) {
            throw new AuthorizationException("Missing Authorization Request Header");
        } else {
            return authHeader.replace("Bearer ", "");
        }
    }

    public UsuarioForum resolve(HttpServletRequest request) throws AuthorizationException {
        String jwtToken = getJwtToken(request);
        if (ValidatorUtil.isEmpty(jwtToken)) {
            throw new AuthorizationException("Token não informado");
        }

        UsuarioForum usuario = JwtValidatorUtils.decodeJWT(jwtToken);

        request.setAttribute(ForumSessionAttributes.USUARIO, usuario);
        request.setAttribute("authToken", jwtToken);

        return usuario;
    }
}
