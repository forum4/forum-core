package br.ufrn.imd.forum.Forum.core.auth.validators;

import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;

public class SenhaValidatorUtils {
    private static final String salt = "#F0rUnnPR0j£t0d3T@lh4d()d&s()f7w@r£";

    public static String encryptPassword(String senha) {
        return Hashing.sha512().hashString(senha.concat(salt), StandardCharsets.UTF_8).toString();
    }
}
