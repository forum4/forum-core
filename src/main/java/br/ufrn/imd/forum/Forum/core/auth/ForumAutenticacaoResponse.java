package br.ufrn.imd.forum.Forum.core.auth;

import br.ufrn.imd.forum.Forum.core.auth.users.UsuarioForum;
import br.ufrn.imd.forum.Forum.enums.TipoPapel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ForumAutenticacaoResponse {
    private UsuarioForum usuario;
    private TipoPapel papel;

    private String errorMessage;
    private String token;
}
