package br.ufrn.imd.forum.Forum.core.auth;

import br.ufrn.imd.forum.Forum.exception.BusinessRuleException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api/v1/auth")
@CrossOrigin(origins = "*", maxAge = 3600)
@RequiredArgsConstructor
public class AutenticacaoRestController {
    private final AutenticacaoService autenticacaoService;

    @PostMapping(path = "/login")
    public ForumAutenticacaoResponse autenticar(@RequestParam("login") String login, @RequestParam("password") String senha, HttpServletResponse response) {
        try {
            return this.autenticacaoService.autenticar(login, senha);
        } catch (BusinessRuleException | ResourceNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }
}
