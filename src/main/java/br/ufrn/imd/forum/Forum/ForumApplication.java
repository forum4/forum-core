package br.ufrn.imd.forum.Forum;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication(scanBasePackages = "br.ufrn.imd.forum", exclude = {FlywayAutoConfiguration.class})
@EnableJpaRepositories(basePackages = "br.ufrn.imd.forum")
@EntityScan("br.ufrn.imd.forum")
@EnableAspectJAutoProxy(proxyTargetClass = true)
@EnableJpaAuditing
@EnableAsync
public class ForumApplication {

	public static void main(String[] args) {
		SpringApplication.run(ForumApplication.class, args);
	}

}
