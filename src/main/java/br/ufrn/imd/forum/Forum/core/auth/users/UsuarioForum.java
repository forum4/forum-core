package br.ufrn.imd.forum.Forum.core.auth.users;

import br.ufrn.imd.forum.Forum.domain.Pessoa;
import br.ufrn.imd.forum.Forum.dto.TipoDTO;
import br.ufrn.imd.forum.Forum.enums.TipoPapel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class UsuarioForum extends TipoDTO<Long> {
    private String usuario;
    private String email;
    private Date dataNascimento;
    private TipoPapel papel;

    public UsuarioForum(Pessoa pessoa) {
        super(pessoa.getId(), pessoa.getNome());
        this.setUsuario(pessoa.getUsuario());
        this.setEmail(pessoa.getEmail());
        this.setPapel(pessoa.getPapel());
        this.setDataNascimento(pessoa.getDataNascimento());
    }
}
