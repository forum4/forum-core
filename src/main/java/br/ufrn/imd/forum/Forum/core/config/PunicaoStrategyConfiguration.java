package br.ufrn.imd.forum.Forum.core.config;

import br.ufrn.imd.forum.Forum.repository.DenunciaRepository;
import br.ufrn.imd.forum.Forum.service.DuvidaService;
import br.ufrn.imd.forum.Forum.service.RespostaService;
import br.ufrn.imd.forum.Forum.service.TopicoForumService;
import br.ufrn.imd.forum.Forum.strategy.punicoes.PunicaoItem;
import br.ufrn.imd.forum.Forum.strategy.punicoes.PunicaoStrategy;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class PunicaoStrategyConfiguration {
    private final TopicoForumService topicoForumService;
    private final DuvidaService duvidaService;
    private final RespostaService respostaService;
    private final DenunciaRepository denunciaRepository;

    @Bean
    public PunicaoStrategy punicaoStrategy() {
        return new PunicaoItem(denunciaRepository, topicoForumService, duvidaService, respostaService);
    }
}
