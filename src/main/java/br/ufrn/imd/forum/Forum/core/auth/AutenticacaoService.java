package br.ufrn.imd.forum.Forum.core.auth;

import br.ufrn.imd.forum.Forum.core.auth.users.UsuarioForum;
import br.ufrn.imd.forum.Forum.core.auth.validators.JwtValidatorUtils;
import br.ufrn.imd.forum.Forum.core.auth.validators.SenhaValidatorUtils;
import br.ufrn.imd.forum.Forum.domain.Pessoa;
import br.ufrn.imd.forum.Forum.exception.BusinessRuleException;
import br.ufrn.imd.forum.Forum.repository.PessoaRepository;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@RequiredArgsConstructor
public class AutenticacaoService {
    private final PessoaRepository pessoaRepository;

    private ForumAutenticacaoResponse createResponse(Pessoa pessoa) {
        ForumAutenticacaoResponse response = new ForumAutenticacaoResponse();

        response.setUsuario(new UsuarioForum(pessoa));
        response.setPapel(pessoa.getPapel());

        Gson gson = new Gson();
        String json = gson.toJson(response.getUsuario());

        response.setToken(JwtValidatorUtils.createJWT(response.getUsuario().getId().toString(), "JWT Issuer", json, 90000000));

        return response;
    }

    public ForumAutenticacaoResponse autenticar(String login, String senha) throws BusinessRuleException, ResourceNotFoundException {
        Pessoa pessoa = this.pessoaRepository.findByEmailOrUsuario(login).orElseThrow(() -> new ResourceNotFoundException("Login ou senha inválido(s)"));

        if (!pessoa.isAtivo()) {
            pessoa.setAtivo(true);
            pessoa.setDataUltimaEdicao(new Date());
            pessoa = this.pessoaRepository.save(pessoa);
        }

        if (!SenhaValidatorUtils.encryptPassword(senha).equals(pessoa.getSenha())) {
            throw new BusinessRuleException("Login ou senha inválido(s)");
        }

        return createResponse(pessoa);
    }
}
